<?php

namespace App\Http\Middleware;

use Closure;

class SelfUpdating
{
    /**
     * User can update his own account. 
     * Admin can update all accounts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authUser = auth()->guard()->user();
        if($authUser->role === "admin") 
            return $next($request);
        
        return ($authUser->id === $request->id) ? 
            $next($request) :
            abort(403, "You don't have any permission to this request!");
    }
}
