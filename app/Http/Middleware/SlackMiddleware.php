<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class SlackMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Session::get('slack_token')) {
            $appId = config('app.slack')['id'];

            return response()->json([
                'redirect' => true,
                'url' => "https://slack.com/oauth/authorize?client_id=$appId"
                    . "&scope=incoming-webhook,channels:write,channels:read"
            ], 401);
        }

        return $next($request);
    }
}
