<?php

namespace App\Http\Middleware;

use Closure;

class MessageFromSubscriber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->guard()->user();

        if(isset($request->conversation_id))
        {
            if ($user->conversations()->find($request->conversation_id)) {
                return $next($request);
            } 

            return response()->json(['error' => "You aren't subscriber of this channel!"], 403);
        }

        return $next($request);
    }
}
