<?php

namespace App\Http\Middleware\Task;

use Closure;

class IsCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $task = $request->route('task');

        if ($task->is_completed) return response()->json(['message' => 'Task has already been completed'], 422);
        return $next($request);
    }
}
