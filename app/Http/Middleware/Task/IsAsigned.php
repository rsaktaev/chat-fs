<?php

namespace App\Http\Middleware\Task;

use Closure;

class IsAsigned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $task = $request->route('task');
        $is_executor = $task->executors()->find(auth()->guard()->user()->id);
        $is_creator = $task->creator->id === auth()->guard()->user()->id;

        if ($is_executor !== null || $is_creator) return $next($request);
        else return response()->json(['message' => 'You are not an executor'], 403);
    }
}
