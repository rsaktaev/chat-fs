<?php

namespace App\Http\Middleware\ChannelMiddleware;

use Closure;
use App\Models\Conversation;

class InviteMembersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authUser = auth()->guard()->user()->id;
        $channel = $request->route('channel');

        if ($channel->members()->find($authUser))
            return $next($request);

        return response()->json(['message' => "You haven't any rights!"], 403);
    }
}
