<?php

namespace App\Http\Middleware\ChannelMiddleware;

use Closure;
use App\Models\Conversation;
use App\Models\User;

class RemoveMembersMiddleware
{
    /**
     * Admin can delete members except himself, other members can delete himself only
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $channel = $request->route('channel');
        $authId = auth()->guard()->user()->id;

        $role = $channel->members()->find($authId)->pivot->role;
        if ($role === 'member') {
            if ($request['member_id'] !== $authId) {
                return response()->json([
                    'message' => "You don't have any rights!"
                ], 403);
            }
        }

        return $next($request);
    }
}
