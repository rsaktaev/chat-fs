<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CheckAdministratorRights
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (auth()->guard()->user()->role === "admin") {
            return $next($request);
        }
        return abort(403, "You don't have any permission to this request!");
    }
}
