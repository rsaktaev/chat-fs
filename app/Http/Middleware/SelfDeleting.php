<?php

namespace App\Http\Middleware;

use Closure;

class SelfDeleting
{
    /**
     * Admin can't delete himself
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if($request->user === auth()->guard()->user()->id)
        {
            return abort(400, "You can't delete yourself!");
        }
        return $next($request);
    }
}
