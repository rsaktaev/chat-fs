<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Http\Requests\CompleteTaskRequest;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Attachment;

class TaskController extends Controller
{
    // getting tasks list
    public function index()
    {
        $user = auth()->guard()->user();
        if ($user->role === 'admin') {
            $tasks = $user->createdTasks()->with(['creator', 'executors'])->orderBy('created_at', 'desc')->get();
        } else {
            $tasks = $user->assignedTasks()->with(['creator', 'executors'])->orderBy('created_at', 'desc')->get();
        }
        return $tasks;
    }

    public function create(TaskRequest $request)
    {
        $task = new Task($request->all());
        $task->creator()->associate($request->creator_id);
        $task->save();

        $task->executors()->attach($request->executors);

        return response()->json(['success' => true]);
    }


    public function complete(Task $task, CompleteTaskRequest $request)
    {
        $task->is_completed = true;
        $task->complete_description = $request->description;
        foreach ($request->attachments as $attachment) {
            $attachmentImage = Attachment::where('id', $attachment['id'])->first();
            if ($attachmentImage == null) return response()->json(['message' => 'Attachment not found'], 404);
            $attachmentImage->attachmentable()->associate($task)->save();
        }

        $task->save();
        return response()->json(['success' => true, 'task' => $task]);
    }
}
