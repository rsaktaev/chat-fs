<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Facades\Session;
use App\Models\Conversation;

class SlackController extends Controller
{

    // Receiving auth code and getting token
    public function access(Request $request)
    {
        $client = new Client();
        $credentials = config('app.slack');
        $response = $client->request('POST', 'https://slack.com/api/oauth.access', [
            'form_params' => [
                'client_id' => $credentials['id'],
                'client_secret' =>  $credentials['secret'],
                'code' => $request->code,
            ]
        ]);

        $response = json_decode($response->getBody(), true);
        if ($response['ok']) {
            Session::put('slack_token', $response['access_token']);
            return redirect('/');
        } else {
            return $response['error'];
        }
    }

    public function index(Request $request)
    {
        //invalid_auth and not_authed
        $token = Session::get('slack_token');
        $client = new Client();

        $response = $client->request('GET', 'https://slack.com/api/conversations.list', [
            'query' => [
                'token' => $token,
                'limit' => $request->limit,
                'cursor' => $request->next,
            ]
        ]);

        $response = json_decode($response->getBody(), true);

        //invalid_auth and not_authed

        if (!$response['ok'])
            if ($response['error'] === 'invalid_auth' || $response['error'] === 'not_authed')
                Session::forget('slack_token');

        return response()->json($response);
    }


    public function export(Request $request)
    {

        //invalid_auth and not_authed
        $token = Session::get('slack_token');
        $client = new Client([
            'content-type' => 'application/json',
        ]);

        $conversation = Conversation::find($request->channelId);
        if ($conversation === null) return response(['ok' => false], 400);

        $response = $client->request('POST', 'https://slack.com/api/channels.create', [
            'form_params' => [
                'token' => $token,
                'name' => $conversation->title,
            ]
        ]);

        $response = json_decode($response->getBody(), true);

        if (!$response['ok'])
            if ($response['error'] === 'invalid_auth' || $response['error'] === 'not_authed')
                Session::forget('slack_token');

        return response()->json($response);
    }
}
