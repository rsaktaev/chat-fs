<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Attachment;
use App\Events\ConversationMessageSended;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\ConversationRequest;
use App\Http\Requests\MemberRequest;
use App\Notifications\PushDemo;
use Illuminate\Support\Facades\Notification;

class ConversationController extends Controller
{
    // getting all conversations
    public function index(Request $request)
    {
        $quantity = isset($request->quantity) ? $request->quantity : 5;
        $conversations = Conversation::where('title', 'like', "%{$request->search}%")
            ->orderBy('created_at', 'desc')
            ->orderBy('title', 'asc')
            ->orderBy('id', 'asc');

        return response()->json([
            'size' => $conversations->count(),
            'offset' => isset($request->skip) ? $request->skip : 0,
            'channels' => $conversations->skip($request->skip)->take($quantity)->get()
        ]);
    }

    // create new conversation
    public function create(ConversationRequest $request)
    {
        $conversation = new Conversation($request->all());
        $conversation->save();
        $conversation->members()->syncWithoutDetaching($request['members']);
        $conversation->members()->syncWithoutDetaching([
            $request['user_id'] => ['role' => 'creator']
        ]);

        return $conversation;
    }

    // getting users conversations
    public function usersConversations(Request $request)
    {
        $authUser = auth()->guard()->user();
        $quantity = isset($request->quantity) ? $request->quantity : 5;
        $conversations = $authUser->conversations()
            ->orderBy('conversation_user.created_at', 'desc');

        return [
            'size' => $conversations->count(),
            'conversations' => $conversations->skip($request->skip)->take($quantity)->get(),
        ];
    }

    // info about channel
    public function show(Conversation $channel)
    {
        $authUser = auth()->guard()->user();

        $user = $authUser->conversations()->find($channel->id);
        $role = $user ? $user->pivot->role : false;

        $conversation = Conversation
            ::with(['messages', 'messages.sender', 'messages.attachments', 'members'])
            ->find($channel->id);
        return response()->json(compact('conversation', 'role'));
    }

    // join/leave a channel
    public function toggle(Conversation $channel)
    {
        $channel->members()->toggle(auth()->guard()->user());
        return response()->json(['success' => 'Success!']);
    }

    // intive to channel
    public function invite(Conversation $channel, MemberRequest $request)
    {
        $channel->members()->syncWithoutDetaching($request['members']);
        return response()->json(['success' => 'Success!']);
    }

    // remove from channel
    public function remove(Conversation $channel, MemberRequest $request)
    {
        $channel->members()->detach($request['members']);
        return response()->json(['success' => 'Success!']);
    }

    // send new message to channel
    public function sendMessage(MessageRequest $request, Conversation $channel)
    {
        try {

            $message = new Message($request->all());

            // retrieving attachments from arrays
            $result = MessageController::retrieveAttachments($request['text'], $request['attachments']);
            $attachments = $result[0];

            $textMessage = MessageController::convertString($result[1]);
            $message->text = $textMessage;

            $message->sender()->associate($request->sender_id);
            $message->receiver()->associate($request->receiver_id);
            $message->conversation()->associate($request->conversation_id);

            $channel->messages()->save($message);

            foreach ($attachments as $file) {
                $attachment = Attachment::find($file['id']);
                $attachment->attachmentable()->associate($message)->save();
            }

            $newMessage = Message::with(['sender', 'attachments'])->find($message->id);
            broadcast(new ConversationMessageSended($channel, $newMessage))->toOthers();

            Notification::send(
                $channel->members()->where('id', '!=', $request->sender_id)->get(),
                new PushDemo($message, $channel->title)
            );

            return $message;
        } catch (Exception $e) {
            return $e;
        }
    }
}
