<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UserController extends Controller
{
    /** 
     * get users list
     * 
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request)
    {
        $quantity = isset($request->quantity) ? $request->quantity : 5;

        $users = User::where('display_name', 'like', "%{$request->pattern}%");

        return response()->json([
            'size' => $users->count(),
            'users' => $users->skip($request->skip)->take($quantity)->withCount(['sendedMessages'])->get(),
        ]);
    }

    /**
     * get information about user
     * 
     * @param \App\Http\User $user - selected user
     * @return \App\Http\User
     */
    public function show(User $user)
    {
        return User::withCount(['sendedMessages'])->find($user->id);
    }

    /**
     * Update user
     * 
     * @param \App\Http\Requests\UserRequest $request - sended data
     * @param \App\Models\User $user - user which we want to update
     * 
     * @return \App\Models\User - updated user
     */
    public function update(UserRequest $request, User $user)
    {
        $user->display_name = $request->display_name;
        $user->full_name = $request->full_name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->avatar_url = $request->avatar_url;

        $user->save();

        return User::where('id', $user->id)->first();
    }

    /**
     * Delete user's account
     * 
     * @param \App\Models\User $user - user which we want to delete
     * 
     */
    public function delete(User $user)
    {
        $user->delete();
        return $user;
    }

    /**
     * Create new user
     * 
     * @param \App\Http\Requests\UserRequest $request - sended data
     * @return \App\Models\User $user - created user
     */
    public function create(UserRequest $request)
    {
        $newUser = new User($request->all());
        $newUser->password = bcrypt(str_random(10));     //will be sended on email

        $newUser->save();

        return $newUser;
    }
}
