<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Auth;
use Exception;
use Lcobucci\JWT\Parser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'phone';
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->guard()->attempt($this->credentials($request))) {
            $user = User::where($this->username(), $request[$this->username()])->first();
            if ($user) {

                $token = $user->createToken('Auth token')->accessToken;
                return response()->json(compact(['user', 'token']));
            } else {
                return response()->json(['error' => true, 'message' => "User doesn't exist"]);
            }
        }

        return response()->json(['error' => true, 'message' => 'Invalid crenedtials!']);
    }

    function logout(Request $request)
    {
        $user = $request->user();
        if ($user === null) return response()->json(['success' => true]);

        $token = $request->bearerToken();
        $id = (new Parser())->parse($token)->getHeader('jti');
        $user->tokens()->find($id)->revoke();

        auth()->guard()->logout();

        return response()->json(['success' => true]);
    }
}
