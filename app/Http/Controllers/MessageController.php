<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MessageRequest;

use App\Models\User;
use App\Models\Message;

use Exception;
use App\Events\DialogMessageSended;
use App\Events\MesagesReaded;
use App\Models\Attachment;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PushDemo;


class MessageController extends Controller
{

    public static $url_match =
    '/((http|https|ftp):\/\/){0,1}[A-Za-zА-яа-я0-9.-]+\.([A-Za-z]{2,}|рф)(\/[^\n\s]*)*/';

    /**
     * Get messages from dialog with user
     * 
     * @param \App\Http\User $user
     * 
     */
    public function getMessagesWithUser(Request $request, User $user)
    {
        try {
            $authUser = auth()->guard()->user();

            //for user's dialog with himself
            if ($user->id === $authUser->id) {
                return Message::where('sender_Id', $user->id)->where('receiver_id', $user->id)->with(['attachments'])->get();
            }

            // scope???
            return Message::where(function ($q) use ($user) {
                $q->where('sender_id',  $user->id)
                    ->orWhere('receiver_id', $user->id);
            })
                ->where(function ($q) use ($authUser, $request) {
                    $q->where('sender_id',  $authUser->id)
                        ->orWhere('receiver_id', $authUser->id);
                })
                ->with(['attachments'])
                ->orderBy('created_at', 'ASC')
                ->get();
        } catch (Exception $e) {
            return response()->json(['error' => 'Cant find user!'], 400);
        }
    }

    public static function convertString($string)
    {
        $result = chop(trim(str_replace("  ", " ", $string)));
        return htmlentities($result);
    }

    public static function retrieveAttachments($text, $attachments)
    {
        preg_match_all(MessageController::$url_match, $text, $matches, PREG_OFFSET_CAPTURE);

        foreach ($matches[0] as $match) {
            try {
                $client = new Client();
                $response = $client->request('GET', $match[0]);
                $contentType = $response->getHeader('content-type')[0];

                if (preg_match('/^image\/.*$/', $contentType)) {
                    $newImage = new Attachment([
                        'name' => $match[0],
                        'mimetype' => $contentType,
                    ]);
                    $newImage->save();
                    $attachments[] = ['id' => $newImage->id];

                    $text = substr_replace($text, "", $match[1], strlen($match[0]));
                }
            } catch (Exception $e) { }
        }

        return [$attachments, $text];
    }

    /**
     * Add new message
     * 
     * @param \App\Http\Requests\MessageRequest $request
     * 
     */
    public function add(MessageRequest $request)
    {

        try {

            $receiver_id = $request->receiver_id;
            $sender_id = $request->sender_id;

            $message = new Message($request->all());
            // retrieving attachments from arrays
            $result = MessageController::retrieveAttachments($request['text'], $request['attachments']);
            $attachments = $result[0];

            $textMessage = MessageController::convertString($result[1]);
            $message->text = $textMessage;
            $message->sender()->associate($sender_id);
            $message->receiver()->associate($receiver_id);
            $message->conversation()->associate($request->conversation_id);

            $message->save();

            foreach ($attachments as $file) {
                $attachment = Attachment::find($file['id']);
                $attachment->attachmentable()->associate($message)->save();
            }

            broadcast(new DialogMessageSended(Message::with(['sender', 'attachments'])->find($message->id)));

            Notification::send(
                User::find($receiver_id),
                new PushDemo($message, $message->sender->display_name)
            );

            return $message;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Sets received messages as readed
     * @param \App\Models\User $user
     */

    public function setAsReaded(User $user)
    {
        $authUser = auth()->guard()->user();
        Message::where('sender_Id', $user->id)
            ->where('receiver_id', $authUser->id)->update(['is_readed' => true]);

        broadcast(new MesagesReaded($authUser->id, $user->id));

        return 'ok';
    }
}
