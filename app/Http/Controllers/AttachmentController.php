<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attachment;

class AttachmentController extends Controller
{

    private $public_folder = 'img/attachments';

    public function save(Request $request)
    {

        $attachment = $request->file('attachments');
        $file_path = public_path($this->public_folder);
        $filename = str_random(10) . ".{$attachment->getClientOriginalExtension()}";
        $mimeType = $attachment->getMimeType();
        $attachment->move($file_path, $filename);

        $newAttachment = new Attachment([
            'name' => $filename,
            'mimetype' => $mimeType,
        ]);

        $newAttachment->save();

        return response()->json(['success' => 'true', 'attachment' => $newAttachment]);
    }
}
