<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'email' => [
                'required',
                'string',
                'max:255',
                'email',
                Rule::unique('users')->ignore($this->id),
            ],

            'phone' => [
                'required',
                'string',
                'max:255',
                Rule::unique('users')->ignore($this->id),
                'regex:/^[0-9]{11}$/'
            ],
            'display_name' => 'required|string|max:255',
            'full_name' => 'required|string|max:255',
        ];
    }

    /**
     * Get the validation messages
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'email.unique' => 'User with this email already exists.',
            'phone.unique' => 'User with this phone number already exists.',
            'phone.regex' => 'Please type phone number with format XXXXXXXXXXX',
        ];
    }
}
