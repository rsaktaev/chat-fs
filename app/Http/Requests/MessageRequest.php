<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender_id' => 'required|integer|exists:users,id',
            'receiver_id' => 'required_without:conversation_id|integer|exists:users,id',
            'conversation_id' => 'required_without:receiver_id|integer|exists:conversations,id',
            'text' => 'required_without:attachments|string|nullable||max:65535',

        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sender_id.exists' => "You can't sending message. Are you registered? ",
            'receiver_id.exists' => "User which you're trying to send message doesn't exist.",
            'text.required_without' => "You can't send empty message.",
            'text.max' => "You exceeded max limit of symbols.",
        ];
    }
}
