<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'string|nullable|max:255',
            'picture_url' => 'nullable|string|max:255',
            'creator_id' => 'required|integer|exists:users,id',
            'executors' => 'required|array',
            'executors.*' => 'integer|exists:users,id',
        ];
    }
}
