<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = [
        'title',
    ];

    //all dialog members
    public function members()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('role')->withTimestamps();
    }

    //all dialog messages
    public function messages()
    {
        return $this->hasMany('App\Models\Message')->orderBy('created_at', 'asc');
    }
}
