<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Conversation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Message extends Model
{

    protected $fillable = [
        'text', 'is_readed'
    ];

    public function setTextAttribute($value)
    {
        return $this->attributes['text'] = Crypt::encrypt($value);
    }

    public function getTextAttribute($value)
    {
        try {
            return Crypt::decrypt($value);
        } catch (Illuminate\Contracts\Encryption\DecryptException $e) {
            return "Error encryption";
        }
    }

    //messages to auth user
    public function scopeToUser($query)
    {
        return $query->where('receiver_id', auth()->user()->id);
    }

    public function scopeUnreaded($query)
    {
        return $query->where('is_readed', false);
    }

    //
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachmentable');
    }
}
