<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //

    protected $fillable = ['title', 'description', 'picture_url'];

    public function creator()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function executors()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function reportPhoto()
    {
        return $this->morphMany('App\Models\Attachment', 'attachmentable');
    }
}
