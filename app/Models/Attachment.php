<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = ['name', 'mimetype'];

    public function attachmentable()
    {
        return $this->morphTo();
    }
}
