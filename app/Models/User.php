<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'phone', 'display_name', 'full_name', 'avatar_url', 'password', 'auth_token', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function conversations()
    {
        return $this->belongsToMany('App\Models\Conversation')->withPivot('role')->withTimestamps();
    }

    public function sendedMessages()
    {
        return $this->hasMany('App\Models\Message', 'sender_id')->toUser()->unreaded();
    }

    // conversations created by user
    public function createdConversations()
    {
        return $this->hasMany('App\Models\Conversation', 'creator_id');
    }

    //tasks which was created by user
    public function createdTasks()
    {
        return $this->hasMany('App\Models\Task', 'creator_id');
    }

    //tasks which was assigned to user
    public function assignedTasks()
    {
        return $this->belongsToMany('App\Models\Task');
    }
}
