<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert admin
        DB::table('users')->insert([
            'email' => 'rsaktaev@gmail.com',
            'phone' => '77777777777',
            'password' => bcrypt('1'),
            'display_name' => 'rsaktaev',
            'full_name' => 'Ruslan Aktaev',
            'status' => 'asleep',
            'role' => 'admin',
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        //insert users
        for ($i = 0; $i < 50; $i++) {
            //insert admin
            DB::table('users')->insert([
                'email' => "{$i}-test@gmail.com",
                'phone' => "7982120321{$i}",
                'password' => bcrypt('1'),
                'display_name' => "user{$i}",
                'full_name' => str_random(5) . " " . str_random(5),
                'status' => "",
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
