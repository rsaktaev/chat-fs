<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Conversation;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class MessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->messagesInDialogs();
        $this->messagesInConversations();
    }

    private function messagesInDialogs()
    {
        // dialog between admin and user with display_name === user1
        $admin = DB::table('users')->where('display_name', 'rsaktaev')->first();
        $firstUser = DB::table('users')->where('display_name', 'user1')->first();
        $secondUser = DB::table('users')->where('display_name', 'user2')->first();

        for ($i = 0; $i < 10; $i++) {

            DB::table('messages')->insert([
                'sender_id' => $firstUser->id,
                'receiver_id' => $admin->id,
                'text' => Crypt::encrypt('Hello, I\'m user!'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            DB::table('messages')->insert([
                'sender_id' => $admin->id,
                'receiver_id' => $firstUser->id,
                'text' => Crypt::encrypt('Hello! I\'m admin'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        // dialog between user1 and user2
        for ($i = 0; $i < 5; $i++) {

            DB::table('messages')->insert([
                'sender_id' => $firstUser->id,
                'receiver_id' => $secondUser->id,
                'text' => Crypt::encrypt('Hello, I\'m user1!'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            DB::table('messages')->insert([
                'sender_id' => $secondUser->id,
                'receiver_id' => $firstUser->id,
                'text' => Crypt::encrypt('Hello! I\'m user2'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        //messages from admin to himself
        for ($i = 0; $i < 5; $i++) {

            DB::table('messages')->insert([
                'sender_id' => $admin->id,
                'receiver_id' => $admin->id,
                'text' => Crypt::encrypt('We are admins!'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }

    private function messagesInConversations()
    {
        $conversation = Conversation::first();
        $admin = User::where('display_name', 'rsaktaev')->first();
        $firstUser = User::where('display_name', 'user1')->first();
        $secondUser = User::where('display_name', 'user2')->first();

        $conversation->messages()->createMany([
            [
                'text' => 'Test message from admin',
                'sender_id' => $admin->id,
            ],
            [
                'text' => 'Test message from user 1',
                'sender_id' => $firstUser->id,
            ],
            [
                'text' => 'Test message from user 2',
                'sender_id' => $secondUser->id,
            ],
        ]);
    }
}
