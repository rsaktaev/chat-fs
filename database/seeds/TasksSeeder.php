<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Task;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where('role', 'admin')->first();
        $firstUser = User::where('display_name', 'user1')->first();
        $secondUser = User::where('display_name', 'user2')->first();

        for ($i = 0; $i < 10; $i++) {
            $firstTask = new Task([
                'title' => 'Throw out the trash',
                'description' => 'so much dirt',
                'picture_url' => 'https://img4.goodfon.ru/wallpaper'
                    . '/nbig/4/f9/nikita-chuntomov-devushka-na-polu-volosy-lokony-musor-fantaz.jpg'
            ]);

            $secondTask = new Task([
                'title' => 'bugagaga some stupid taskkkkk!!!!!!!!!!',
                'description' => 'aaaaree you getting bored????????? orly??' .
                    'i can help youuuuuu!!! hahahhaha',
            ]);

            $firstTask->creator()->associate($admin->id);
            $firstTask->save();
            $firstTask->executors()->attach([$firstUser->id, $secondUser->id]);

            $secondTask->creator()->associate($admin->id);
            $secondTask->save();
            $secondTask->executors()->attach([$firstUser->id]);
        }
    }
}
