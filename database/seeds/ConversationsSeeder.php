<?php

use Illuminate\Database\Seeder;
use App\Models\Conversation;
use App\Models\User;

class ConversationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::where('display_name', 'rsaktaev')->first();
        $firstUser = User::where('display_name', 'user1')->first();
        $secondUser = User::where('display_name', 'user2')->first();

        for ($i = 0; $i < 15; $i++) {
            $conversation = new Conversation(['title' => "test channel #$i"]);
            $conversation->save();

            $conversation->members()->attach([
                $admin->id => ['role' => 'creator'],
            ]);

            $conversation->members()->attach([$firstUser->id, $secondUser->id,]);
        }
    }
}
