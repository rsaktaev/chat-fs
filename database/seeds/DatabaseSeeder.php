<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(ConversationsSeeder::class);
        $this->call(MessagesSeeder::class);
        $this->call(TasksSeeder::class);
    }
}
