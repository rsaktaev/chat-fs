## Запсук проекта

### Установка зависимостей Backend
```
composer install
```
### Генерация ключей для Service Worker (пушей в браузер)
```
php artisan webpush:vapid
```

В .env нужно добавить следующую строку:
```
MIX_VAPID_PUBLIC_KEY="${VAPID_PUBLIC_KEY}"
```

Также нужно сгенерировать ключи для Pusher (https://pusher.com/)

### Накатывание миграций
```
php artisan migrate:refresh --seed
php artisan passport:install
```

### Compiling Assets (frontend) 
```
npm install
npm run dev
```