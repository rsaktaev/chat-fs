<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/slack/callback', 'SlackController@access');    //redirect uri 

Route::middleware(['auth:api'])->group(function () {

    //interaction with slack API
    Route::group(['prefix' => 'slack', 'middleware' => 'slack'], function () {
        Route::get('/channels', 'SlackController@index');
        Route::post('/channels/new', 'SlackController@export');
    });

    Route::get('/users', 'UserController@index');
    Route::get('/users/{user}', 'UserController@show');
    Route::put('/users/{user}', 'UserController@update')->middleware('update');

    Route::get('/messages/{user}', 'MessageController@getMessagesWithUser');
    Route::post('/messages', 'MessageController@add')->middleware('subscriber');
    Route::put('/messages/readed/{user}', 'MessageController@setAsReaded');

    Route::group(['middleware' => 'admin', 'prefix' => 'users'], function () {
        Route::post('/', 'UserController@create');
        Route::delete('/{user}', 'UserController@delete')->middleware('self');
    });

    Route::group(['prefix' => 'conversations'], function () {

        Route::get('/', 'ConversationController@index');
        Route::get('/my', 'ConversationController@usersConversations');
        Route::get('/{channel}', 'ConversationController@show');

        Route::put('/{channel}/toggle', 'ConversationController@toggle');

        Route::put('/{channel}/invite', 'ConversationController@invite')->middleware(['invite']);
        Route::put('/{channel}/remove', 'ConversationController@remove')->middleware(['remove']);

        Route::post('/', 'ConversationController@create');
        Route::post('/{channel}/send', 'ConversationController@sendMessage');
    });

    Route::post('/attachments', 'AttachmentController@save');

    // tasks for users
    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/', 'TaskController@index');
        Route::post('/', 'TaskController@create')->middleware(['admin']);
        Route::put('/{task}/complete', 'TaskController@complete')->middleware(['task_accomplishion']);
    });
});
