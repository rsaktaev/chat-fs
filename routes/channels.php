<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int)$user->id === (int)$id;
});

Broadcast::channel('message.{channelId}', function ($user, $channelId) {
    return true;
});

Broadcast::channel('dialog.message.{senderId}.{receiverId}', function ($user, $senderId, $receiverId) {
    return ((int)$user->id === (int)$receiverId) || ((int)$user->id === (int)$senderId);
});
