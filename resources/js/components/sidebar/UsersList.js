import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'
import {
  MDBContainer,
  MDBListGroup,
  MDBListGroupItem,
  MDBIcon,
  MDBBadge,
} from 'mdbreact'
import { loadUsers } from '../../api'
import { OFFSET } from '../../constants'

const UsersList = props => {
  const [users, setUsers] = useState(props.users.users)
  const [size, setSize] = useState(props.users.size)
  const [offset, setOffset] = useState(OFFSET)

  const _onSuccess = res => {
    const responseData = res.data
    if (!responseData.error) {
      setUsers(users => [...users, ...responseData.users])
      setSize(responseData.size)

      setOffset(offset => offset + OFFSET)
      props.setStatus({
        loadingError: '',
        isLoading: false,
      })
    } else {
      props.setStatus({
        isLoading: false,
      })
      props.onLogout()
    }
  }

  const _onError = error => {
    console.log(error.response)
    if (error.response && error.response.status === 401) {
      props.onLogout()
    } else {
      props.setStatus({
        loadingError: 'Server error',
        isLoading: false,
      })
    }
  }

  const handleLoadingMore = event => {
    event.preventDefault()
    loadUsers(_onSuccess, _onError, offset)
  }

  return (
    <div>
      <MDBContainer>
        <h4 className="mx-auto mb-3">
          Members{' '}
          <small>
            <MDBIcon className="ml-1" icon="user-alt" />
          </small>{' '}
        </h4>
      </MDBContainer>
      <MDBListGroup>
        {users.map(user => (
          <Link to={`/dialog/${user.id}`} key={user.id}>
            <MDBListGroupItem
              className={`list__element${
                props.matchPath(user) ? ' active' : ''
              }`}
              hover
              onClick={props.handleContactClick}
            >
              {user.display_name}{' '}
              {user.sended_messages_count > 0 && (
                <MDBBadge pill size="3x" color="default">
                  {user.sended_messages_count}
                </MDBBadge>
              )}
            </MDBListGroupItem>
          </Link>
        ))}
      </MDBListGroup>
      {size > offset && (
        <button className="btn btn-sm" onClick={handleLoadingMore}>
          load more...
        </button>
      )}
    </div>
  )
}

UsersList.propTypes = {
  users: PropTypes.shape({
    users: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
  }).isRequired,
  matchPath: PropTypes.func.isRequired,
  handleContactClick: PropTypes.func.isRequired,
  setStatus: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
}

export default UsersList
