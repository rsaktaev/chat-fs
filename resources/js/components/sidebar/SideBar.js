import React from 'react'

import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'
import '../../../css/sidebar/SideBar.css'

import { MDBView, MDBMask, MDBIcon, MDBContainer } from 'mdbreact'
import UsersList from './UsersList'
import UsersChannels from './UsersChannels'

class SideBar extends React.Component {
  activeElement = null

  //if current path matches with dialog path
  matchPath = user =>
    new RegExp(`^/dialog/${user.id}([^0-9.]+.*)*$`).test(
      this.props.locationPath
    )

  handleLogout = event => {
    event.preventDefault()
    this.props.onLogout()
  }

  handleContactClick = event => {
    this.props.collapseSidebar(true)
  }

  render() {
    const {
      user,
      isOpened,
      usersList,
      conversations,
      setStatus,
      onLogout,
    } = this.props
    const { display_name, avatar_url } = user

    return (
      <div>
        <div className={`navbar-dark sidebar${isOpened ? ' opened' : ''}`}>
          <button
            className="collapse__sidebar btn btn-link navbar-toggler d-md-none"
            onClick={() => this.props.collapseSidebar()}
          >
            X
          </button>

          <div className="user__info mb-5">
            <MDBView hover id="avatar" className="mx-auto avatar">
              <img
                src={avatar_url}
                alt="avatar"
                className="rounded-circle"
                width="100"
              />
              <MDBMask className="flex-center" overlay="black-light">
                <Link to="/settings">
                  <MDBIcon icon="edit" />
                </Link>
              </MDBMask>
            </MDBView>
            <div className="mx-auto" id="name">
              {display_name}
            </div>
            <div className="mx-auto">
              <Link to="/settings">
                Settings <MDBIcon icon="cog" />
              </Link>
            </div>
          </div>

          <Link to="/tasks">
            <MDBContainer>
              <h4>
                Tasks <MDBIcon icon="tasks" />
              </h4>
            </MDBContainer>
          </Link>
          <UsersChannels
            conversations={conversations}
            setStatus={setStatus}
            handleContactClick={this.handleContactClick}
          />
          <UsersList
            users={usersList}
            setStatus={setStatus}
            matchPath={this.matchPath}
            onLogout={onLogout}
            handleContactClick={this.handleContactClick}
          />
          <div className="mt-5">
            <button className="btn logout__btn" onClick={this.handleLogout}>
              Logout
            </button>
          </div>
        </div>
        <div className={`bg${isOpened ? ' opened' : ''}`} />
      </div>
    )
  }
}

SideBar.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
    display_name: PropTypes.string.isRequired,
    full_name: PropTypes.string.isRequired,
    avatar_url: PropTypes.string.isRequired,
    isOpened: PropTypes.bool,
    collapseSidebar: PropTypes.func,
  }).isRequired,
  conversations: PropTypes.shape({
    conversations: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
  }),
  onLogout: PropTypes.func.isRequired,
  usersList: PropTypes.shape({
    users: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
  }).isRequired,
  setStatus: PropTypes.func.isRequired,
}

export default SideBar
