import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { MDBContainer, MDBListGroup, MDBListGroupItem, MDBIcon } from 'mdbreact'
import { Link } from 'react-router-dom'
import { OFFSET } from '../../constants'
import { loadConversations } from '../../api'

const UsersChannels = React.memo(props => {
  const [conversations, setConversations] = useState(
    props.conversations.conversations
  )

  const [size, setSize] = useState(props.conversations.size)
  const [offset, setOffset] = useState(OFFSET)

  if (props.conversations.size !== size) {
    setConversations(props.conversations.conversations)
    setOffset(OFFSET)
    setSize(props.conversations.size)
  }

  const _onSuccess = res => {
    const responseData = res.data
    if (!responseData.error) {
      setConversations(conversations => [
        ...conversations,
        ...responseData.conversations,
      ])
      setSize(responseData.size)
      setOffset(offset => offset + OFFSET)

      props.setStatus({
        loadingError: '',
        isLoading: false,
      })
    } else {
      props.setStatus({
        isLoading: false,
      })
      props.onLogout()
    }
  }

  const _onError = error => {
    console.log(error.response)
    if (error.response && error.response.status === 401) {
      props.onLogout()
    } else {
      props.setStatus({
        loadingError: 'Server error',
        isLoading: false,
      })
    }
  }

  const handleLoadingMore = event => {
    event.preventDefault()
    loadConversations(_onSuccess, _onError, offset)
  }

  return (
    <div className="mb-4">
      <MDBContainer>
        <Link to="/channels">
          <h4 className="mx-auto mb-3 channels_title">
            Channels{' '}
            <small className="ml-1">
              <MDBIcon icon="users" />
            </small>
            <small className="ml-1">
              <MDBIcon icon="plus-circle" />
            </small>
          </h4>
        </Link>
      </MDBContainer>
      <MDBListGroup>
        {conversations.map(conversation => (
          <Link to={`/channels/${conversation.id}`} key={conversation.id}>
            <MDBListGroupItem
              hover
              className="list__element"
              key={conversation.id}
              onClick={props.handleContactClick}
            >
              {conversation.title}
            </MDBListGroupItem>
          </Link>
        ))}
      </MDBListGroup>
      {size > offset && (
        <button className="btn btn-sm" onClick={handleLoadingMore}>
          load more...
        </button>
      )}
    </div>
  )
})

UsersChannels.propTypes = {
  setStatus: PropTypes.func.isRequired,
  conversations: PropTypes.shape({
    conversations: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
  }),
  handleContactClick: PropTypes.func.isRequired,
}

export default UsersChannels
