import React from 'react'
import { Route, Redirect } from 'react-router-dom'

export const AuthRoute = props => {
  if (localStorage.getItem('token')) {
    return <Route {...props} />
  } else {
    sessionStorage.setItem('intendedUrl', props.location.pathname)
    return <Redirect to="/login" />
  }
}
