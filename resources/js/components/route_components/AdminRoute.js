//route only for admins
import React from 'react'
import { Route, Redirect } from 'react-router-dom'

export const AdminRoute = props => {
  const user = JSON.parse(localStorage.getItem('user'))

  if (!user) return <Redirect to="/login" />
  else return user.role === 'admin' ? <Route {...props} /> : <div>404</div>
}
