import React from 'react'
import { Route, Redirect } from 'react-router-dom'

export const NonAuthRoute = props => {
  return localStorage.getItem('token') ? (
    <Redirect to="/" />
  ) : (
    <Route {...props} />
  )
}
