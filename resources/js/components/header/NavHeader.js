import React from 'react'
import { Navbar, NavbarToggler } from 'mdbreact'

import '../../../css/header/NavHeader.css'

import PropTypes from 'prop-types'

class NavHeader extends React.Component {
  render = () => (
    <header className="nav__header d-md-none">
      <Navbar className="navbar" dark>
        <NavbarToggler onClick={() => this.props.collapseSidebar()} />
      </Navbar>
    </header>
  )
}

NavHeader.propTypes = {
  collapseSidebar: PropTypes.func.isRequired,
}

export default NavHeader
