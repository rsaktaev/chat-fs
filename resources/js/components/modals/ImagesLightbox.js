import React, { useState } from 'react'
import PropTypes from 'prop-types'
import '../../../css/modals/ImagesLightbox.css'
import { MDBIcon } from 'mdbreact'

const ImagesLightbox = ({ toggle, displayIndex, imagesList }) => {
  const [index, setIndex] = useState(displayIndex)

  const handleClose = event => {
    toggle()
  }

  const handleNavLeft = event => {
    if (index > 0) {
      setIndex(index => --index)
    }
    event.stopPropagation()
  }

  const handleNavRight = event => {
    if (index < imagesList.length - 1) {
      setIndex(index => ++index)
    }
    event.stopPropagation()
  }

  return (
    <div className="modal_container dialog_lightbox__container">
      <div id="lightbox" className="dialog_lightbox" onClick={handleClose}>
        <MDBIcon
          id="close_btn"
          className="dialog_lightbox__button close_btn text-light"
          icon="times"
        />
        <MDBIcon
          className={
            'dialog_lightbox__button left_btn text-light' +
            (index <= 0 ? ' disabled' : '')
          }
          icon="chevron-left"
          onClick={handleNavLeft}
        />
        <MDBIcon
          className={
            'dialog_lightbox__button right_btn text-light' +
            (index >= imagesList.length - 1 ? ' disabled' : '')
          }
          icon="chevron-right"
          onClick={handleNavRight}
        />
        <img
          src={
            /^(http|https|ftp):\/\/.*/.test(imagesList[index].name)
              ? imagesList[index].name
              : `/img/attachments/${imagesList[index].name}`
          }
          alt="From dialog"
        />
      </div>
    </div>
  )
}

ImagesLightbox.propTypes = {
  toggle: PropTypes.func.isRequired,
  displayIndex: PropTypes.number.isRequired,
  imagesList: PropTypes.array.isRequired,
}

export default ImagesLightbox
