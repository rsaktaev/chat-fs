import React from 'react'
import PropTypes from 'prop-types'
import DialogHeader from './DialogHeader'
import Dialog from './Dialog'
import NewMessage from '../forms/NewMessage'
import ImagesLightbox from '../modals/ImagesLightbox'

import '../../../css/dialog/DialogContainer.css'

import {
  loadDialogMessages,
  sendMessageToUser,
  loadDialogInfo,
  setAsReaded,
} from '../../api'
import { MDBAlert } from 'mdbreact'

class DialogContainer extends React.Component {
  state = {
    interlocutorId: null,
    interlocutor: {},
    dialogMessages: [],
    shouldFetch: false,
    sendErrorMessage: '',
    loadDialogErrorMessages: '',
    isLoading: true,
    sendingIsLoading: false,
    lightboxIsOpen: false,
    displayIndex: 0,
    imagesList: [],
    typing: null,
  }

  // load messages only when interlocutor is different
  static getDerivedStateFromProps(props, state) {
    if (props.match.params.id === state.interlocutorId)
      return {
        shouldFetch: false,
      }

    return {
      interlocutorId: props.match.params.id,
      shouldFetch: true,
    }
  }

  onLoadingDialogInfoSuccess = response => {
    if (response.data.error) {
      this.setState({
        loadDialogErrorMessages: `${response.data.error}`,
        isLoading: false,
      })
      this.props.onLogout()
    } else {
      const interlocutor = response.data
      this.setState({ interlocutor })
      return interlocutor
    }
  }

  onLoadingDialogInfoError = error => {
    const errorResponse = error.response
    if (errorResponse && errorResponse.data) {
      if (errorResponse.status === 401) {
        this.props.onLogout()
      } else {
        if (errorResponse.status === 404) {
          this.setState({
            loadDialogErrorMessages: `Error! User not found.`,
            isLoading: false,
            shouldFetch: false,
          })
        } else {
          this.setState({
            loadDialogErrorMessages: `Server error`,
            isLoading: false,
            shouldFetch: false,
          })
        }
      }
    } else {
      this.setState({
        loadDialogErrorMessages: "Can't load dialog!",
        isLoading: false,
        shouldFetch: false,
      })
    }
  }

  /**
   * Loading userinfo and messages
   *
   * @return {void}
   */
  loadDialogInfo() {
    const { match, token, loadUsers } = this.props
    const interlocutorId = match.params.id
    const {
      onLoadingDialogInfoSuccess,
      onLoadingDialogInfoError,
      onLoadingMessagesSuccess,
      onLoadingMessagesError,
    } = this

    this.setState({ isLoading: true })

    loadDialogInfo(
      interlocutorId,
      token,
      onLoadingDialogInfoSuccess,
      onLoadingDialogInfoError
    )
      .then(interlocutor => {
        if (interlocutor.sended_messages_count > 0) {
          setAsReaded(token, interlocutorId, () => loadUsers(), () => {})
        }
        return interlocutor
      })
      .then(interlocutor =>
        loadDialogMessages(
          interlocutor,
          token,
          onLoadingMessagesSuccess,
          onLoadingMessagesError
        )
      )
  }

  onLoadingMessagesSuccess = response => {
    const responseData = response.data

    if (responseData.error) {
      this.setState({
        loadDialogErrorMessages: `${responseData.error}`,
        isLoading: false,
        shouldFetch: false,
      })

      this.props.onLogout()
    } else {
      this.setState({
        dialogMessages: responseData,
        sendErrorMessage: '',
        loadDialogErrorMessages: '',
        shouldFetch: false,
        isLoading: false,
      })
    }
  }

  onLoadingMessagesError = error => {
    const errorResponse = error.response
    if (errorResponse && errorResponse.data) {
      if (errorResponse.status === 401) {
        this.props.onLogout()
      } else {
        if (errorResponse.status === 404) {
          this.setState({
            loadDialogErrorMessages: `Error! User not found.`,
            isLoading: false,
            shouldFetch: false,
          })
        } else {
          this.setState({
            loadDialogErrorMessages: `Server error`,
            isLoading: false,
            shouldFetch: false,
          })
        }
      }
    } else {
      this.setState({
        loadDialogErrorMessages: "Can't load dialog!",
        isLoading: false,
        shouldFetch: false,
      })
    }
  }

  /**
   * Loading messages with user
   */
  loadMessages = () =>
    loadDialogMessages(
      this.state.interlocutor,
      this.props.token,
      this.onLoadingMessagesSuccess,
      this.onLoadingMessagesError
    )

  onSendingMessageSuccess = response => {
    console.log(response)
    if (response.status === 201) {
      this.loadMessages()
      this.setState({ sendingIsLoading: false })
    } else
      this.setState({
        sendErrorMessage: "Can't send message.",
        sendingIsLoading: false,
      })
  }

  onSendingMessageError = error => {
    const errorResponse = error.response

    if (errorResponse) {
      if (errorResponse.data.errors)
        this.setState({
          sendErrorMessage: errorResponse.data.errors.text
            ? errorResponse.data.errors.text[0]
            : 'Something is wrong',
          sendingIsLoading: false,
        })
      else
        this.setState({
          sendErrorMessage: "Can't send message. Server error.",
          sendingIsLoading: false,
        })
    } else {
      this.setState({
        sendErrorMessage: "Can't send message. Something is wrong.",
        sendingIsLoading: false,
      })
    }
  }

  /**
   * Sending a new message
   *
   *  */
  sendMessage = (text, attachments) => {
    const { user, match, token } = this.props
    const interlocutorId = match.params.id

    this.setState({ sendingIsLoading: true })

    sendMessageToUser(
      interlocutorId,
      text,
      attachments,
      user,
      token,
      this.onSendingMessageSuccess,
      this.onSendingMessageError
    )
  }

  toggleLightbox = (index, imagesList) =>
    this.setState(prevState => ({
      lightboxIsOpen: !prevState.lightboxIsOpen,
      displayIndex: index,
      imagesList: imagesList,
    }))

  render() {
    const { user, match } = this.props
    const {
      dialogMessages,
      interlocutor,
      sendErrorMessage,
      loadDialogErrorMessages,
      isLoading,
      sendingIsLoading,
      lightboxIsOpen,
      displayIndex,
      imagesList,
      typing,
    } = this.state

    return (
      <React.Fragment>
        {!isLoading ? (
          loadDialogErrorMessages ? (
            <MDBAlert color="danger">{loadDialogErrorMessages}</MDBAlert>
          ) : (
            <div className="dialog__container">
              <DialogHeader
                display_name={interlocutor.display_name}
                avatar_url={interlocutor.avatar_url}
              />
              <Dialog
                lightboxIsOpen={lightboxIsOpen}
                messages={dialogMessages}
                toggleLightbox={this.toggleLightbox}
                user={user}
                typing={typing}
              />
              <NewMessage
                user={user}
                channelpath={`dialog.message.${user.id}.${match.params.id}`}
                isLoading={sendingIsLoading}
                sendMessage={this.sendMessage}
                errorMessage={sendErrorMessage}
                token={this.props.token}
              />
              {lightboxIsOpen && (
                <ImagesLightbox
                  toggle={this.toggleLightbox}
                  displayIndex={displayIndex}
                  imagesList={imagesList}
                />
              )}
            </div>
          )
        ) : (
          <div className="loader__wrapper">
            <div className="spinner-border">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
      </React.Fragment>
    )
  }

  listenChannel = (sender_id, receiver_id) => {
    window.Echo.private(`dialog.message.${sender_id}.${receiver_id}`)
      .listen('.send', e => {
        const { token, match } = this.props
        this.setState(prevState => {
          prevState.dialogMessages.push(e.message)
          return { dialogMessages: prevState.dialogMessages }
        })
        setAsReaded(token, match.params.id, () => this.loadMessages(), () => {})
      })
      .listen('.read', e => {
        this.loadMessages()
      })
      .listenForWhisper('typing', e => {
        if (!this.state.typing) {
          this.setState({ typing: e.userName })
          setTimeout(() => this.setState({ typing: '' }), 2000)
        }
      })
  }

  componentDidMount() {
    this.loadDialogInfo()
    this.listenChannel(this.props.match.params.id, this.props.user.id)
  }

  componentDidUpdate(prevProps) {
    if (this.state.shouldFetch) {
      this.loadDialogInfo()

      window.Echo.leave(
        `dialog.message.${prevProps.match.params.id}.${prevProps.user.id}`
      )
      this.listenChannel(this.props.match.params.id, this.props.user.id)
    }
  }

  componentWillUnmount() {
    window.Echo.leave(
      `dialog.message.${this.props.match.params.id}.${this.props.user.id}`
    )
  }
}

export default DialogContainer

DialogContainer.propTypes = {
  user: PropTypes.object.isRequired,
  loadUsers: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
}
