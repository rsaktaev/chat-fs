import React from 'react'

import '../../../css/dialog/Dialog.css'

import ReactHtmlParser from 'react-html-parser'
import PropTypes from 'prop-types'
import { MDBIcon } from 'mdbreact'
import { formattedTime, formatMessage } from '../../utils'
class Dialog extends React.Component {
  messageContainer = React.createRef()

  //split array of images and files
  //TODO - вынести функцию!
  filterFiles = attachments => {
    const images = [],
      files = []
    attachments.forEach(element => {
      if (/^image\/.+$/.test(element.mimetype)) {
        images.push(element)
      } else {
        files.push(element)
      }
    })

    return [images, files]
  }

  renderMessages(messages) {
    return messages.map(message => {
      const { id, sender_id, text, attachments, is_readed } = message
      const formattedText = formatMessage(text) //add links
      const time = new Date(message.created_at)

      const [images, files] = this.filterFiles(attachments)
      const isSended = sender_id === this.props.user.id

      return (
        <div
          key={id}
          className={isSended ? `sendedMessage` : 'receivedMessage'}
        >
          {!is_readed ? (
            <div className="not_readed__wrapper">
              <MDBIcon className="not_readed" icon="circle" />
            </div>
          ) : (
            false
          )}
          <div className="message_text">
            {ReactHtmlParser(formattedText)}
            <div className="message__images">
              {images.map((element, index) => (
                <img
                  key={element.id}
                  src={
                    /^(http|https|ftp):\/\/.*/.test(element.name)
                      ? element.name
                      : `/img/attachments/${element.name}`
                  }
                  alt="attachment"
                  onClick={() => this.props.toggleLightbox(index, images)}
                />
              ))}
            </div>
            <div>
              {files.map(element => (
                <div key={element.id}>
                  <a href={`/img/attachments/${element.name}`}>
                    <MDBIcon icon="paperclip" /> {element.name}
                  </a>
                </div>
              ))}
            </div>
          </div>
          <div className="time">
            <small>{formattedTime(time)}</small>
          </div>
        </div>
      )
    })
  }

  render() {
    const { messages, typing } = this.props
    return (
      <div className="message__wrapper" ref={this.messageContainer}>
        <div className="message__container">
          {this.renderMessages(messages)}
        </div>
        {typing && `${typing} is typing...`}
      </div>
    )
  }

  //scroll dialog to the bottom after render
  scrollToBottom = messageContainer => {
    let clientHeight = messageContainer.clientHeight
    let scrollHeight = messageContainer.scrollHeight

    console.log('client height is ' + clientHeight)
    console.log('scroll heightn is ' + scrollHeight)

    messageContainer.scrollTop = scrollHeight - clientHeight
  }

  componentDidMount() {
    let messageContainer = this.messageContainer.current
    setTimeout(() => this.scrollToBottom(messageContainer), 200)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.lightboxIsOpen === this.props.lightboxIsOpen) {
      let messageContainer = this.messageContainer.current
      setTimeout(() => this.scrollToBottom(messageContainer), 200)
    }
  }
}

Dialog.propTypes = {
  lightboxIsOpen: PropTypes.bool.isRequired,
  messages: PropTypes.array.isRequired,
  toggleLightbox: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
}

export default Dialog
