import React from 'react'
import PropTypes from 'prop-types'

import '../../../css/dialog/DialogHeader.css'

const DialogHeader = ({ display_name, avatar_url }) => (
  <div className="dialog__header">
    <div>
      <b>
        <a href="#test">{display_name}</a>
      </b>
    </div>
    <div className="avatar">
      <img
        src={avatar_url}
        alt="avatar"
        className="rounded-circle"
        width="40"
      />
    </div>
  </div>
)

DialogHeader.propTypes = {
  display_name: PropTypes.string.isRequired,
  avatar_url: PropTypes.string.isRequired,
}

export default DialogHeader
