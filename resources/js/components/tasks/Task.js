import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBIcon,
  MDBBtn,
} from 'mdbreact'
import CompleteForm from '../forms/CompleteForm'

const Task = ({ task, manuallyRedraw, token }) => {
  const [completeIsOpen, setCompleteIsOpen] = useState(false)
  const [_task, setTask] = useState(task)

  const handleComplete = event => {
    event.preventDefault()
    setCompleteIsOpen(prevState => !prevState)
  }

  useEffect(() => manuallyRedraw())

  const renderFooter = () => {
    if (!_task.is_completed)
      return (
        <React.Fragment>
          {completeIsOpen && (
            <CompleteForm
              taskId={id}
              token={token}
              setTask={setTask}
              manuallyRedraw={manuallyRedraw}
            />
          )}
          {collapseButtonText}
        </React.Fragment>
      )
    return (
      <div>
        <div>{_task.complete_description}</div>
      </div>
    )
  }

  const { id, picture_url, title, description, is_completed } = _task
  const collapseButtonText = completeIsOpen ? (
    <MDBBtn
      href="#"
      color="danger"
      className="btn-block"
      onClick={handleComplete}
    >
      Cancel
    </MDBBtn>
  ) : (
    <MDBBtn href="#" className="btn-block" onClick={handleComplete}>
      I want to complete
    </MDBBtn>
  )

  return (
    <MDBCard style={{ width: '22rem' }}>
      <MDBCardImage
        className="img-fluid"
        src={
          /^(http|https|ftp):\/\/.*/.test(picture_url)
            ? picture_url
            : `/img/attachments/${picture_url}`
        }
      />
      <MDBCardBody>
        <MDBCardTitle>
          {title}{' '}
          {is_completed && (
            <MDBIcon icon="check" style={{ color: '#88DE0D' }} />
          )}
        </MDBCardTitle>
        <MDBCardText>{description}</MDBCardText>
        {renderFooter()}
      </MDBCardBody>
    </MDBCard>
  )
}

export default Task

Task.propTypes = {
  task: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
  manuallyRedraw: PropTypes.func.isRequired,
}
