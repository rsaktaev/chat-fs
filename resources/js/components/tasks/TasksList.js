import React from 'react'
import PropTypes from 'prop-types'
import Task from './Task'
import Masonry from 'react-masonry-component'
import '../../../css/tasks/TasksList.css'

const TasksList = ({ tasks, token }) => {
  let masonry = null

  const manuallyRedraw = () => {
    masonry.layout()
  }

  return (
    <Masonry
      className={'tasks-grid'}
      ref={c => (masonry = masonry || c.masonry)}
      options={{ gutter: 10 }}
    >
      {tasks.map(task => (
        <Task
          key={task.id}
          task={task}
          manuallyRedraw={manuallyRedraw}
          token={token}
        />
      ))}
    </Masonry>
  )
}

export default TasksList

TasksList.propTypes = {
  tasks: PropTypes.array.isRequired,
  token: PropTypes.string.isRequired,
}
