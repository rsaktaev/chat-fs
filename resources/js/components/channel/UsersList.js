import React, { useReducer, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'

import {
  MDBListGroup,
  MDBListGroupItem,
  MDBAlert,
  MDBBtn,
  MDBIcon,
} from 'mdbreact'
import { loadUsers } from '../../api'
import { OFFSET } from '../../constants'

const reducer = (state, action) => {
  switch (action.type) {
    case 'USERS_LOADING_REQUEST':
      return { ...state, isLoading: true }
    case 'USERS_LOADING_SUCCESS':
      return {
        ...state,
        users: {
          users: [...state.users.users, ...action.payload.users],
          size: action.payload.size,
        },
        offset: state.offset + OFFSET,
        isLoading: false,
        errorMessage: '',
      }
    case 'USERS_LOADING_SEARCH':
      return {
        ...state,
        users: {
          users: [...action.payload.users],
          size: action.payload.size,
        },
        offset: OFFSET,
        isLoading: false,
        errorMessage: '',
      }
    case 'USERS_LOADING_ERROR':
      return { ...state, errorMessage: action.payload, isLoading: false }
    case 'INVITE_USER':
      return { ...state, members: action.payload }
    default:
      return state
  }
}

const UsersList = ({ members, join }) => {
  const membersSet = new Set(members)
  const search = useRef()
  const [state, dispatch] = useReducer(reducer, {
    isLoading: true,
    errorMessage: null,
    users: { users: [], size: 0 },
    members: new Set(),
    offset: 0,
    search: '',
  })

  const onUserSelect = event => {
    const listItem = event.target

    const currentMembers = new Set(state.members)
    const value = parseInt(listItem.dataset.value)
    if (!listItem.classList.contains('active')) currentMembers.add(value)
    else currentMembers.delete(value)

    dispatch({ type: 'INVITE_USER', payload: currentMembers })
  }

  const renderUsersList = users =>
    users.users.map(element => {
      return (
        <MDBListGroupItem
          hover
          onClick={onUserSelect}
          key={element.id}
          data-value={element.id}
          className={`form-check${
            membersSet.has(element.id) ? ' disabled' : ''
          }${state.members.has(element.id) ? ' active' : ''}`}
        >
          {element.display_name}
        </MDBListGroupItem>
      )
    })

  const onSearchSuccess = response => {
    const data = response.data
    if (data) {
      dispatch({
        type: 'USERS_LOADING_SEARCH',
        payload: data,
      })
    } else {
      dispatch({
        type: 'USERS_LOADING_ERROR',
        payload: 'Error!',
      })
    }
  }

  const onLoadingUsersSuccess = response => {
    const data = response.data
    if (data) {
      dispatch({
        type: 'USERS_LOADING_SUCCESS',
        payload: data,
      })
    } else {
      dispatch({
        type: 'USERS_LOADING_ERROR',
        payload: 'Error!',
      })
    }
  }

  const onLoadingUsersError = error => {
    if (error.response) {
      dispatch({
        type: 'USERS_LOADING_ERROR',
        payload: 'Error!',
      })
    } else {
      dispatch({
        type: 'USERS_LOADING_ERROR',
        payload: 'Server error!',
      })
    }
  }

  useEffect(() => loadUsers(onLoadingUsersSuccess, onLoadingUsersError), [])

  const handleSubmit = event => join([...state.members])

  const { users, errorMessage, offset } = state

  const onSearch = event => {
    const search = event.target.value
    loadUsers(onSearchSuccess, onLoadingUsersError, 0, search)
  }

  return (
    <div>
      <h2 className="mt-5 mb-3">Invite users</h2>
      <div className="form-group input-group">
        <input ref={search} className="form-control" onChange={onSearch} />
        <div className="input-group-append">
          <div className="input-group-text">
            <MDBIcon icon="search" />
          </div>
        </div>
      </div>
      {errorMessage ? (
        <MDBAlert color="danger">{errorMessage}</MDBAlert>
      ) : (
        <>
          <div className="list">
            <MDBListGroup>{renderUsersList(users)}</MDBListGroup>
          </div>
          {users.size > offset && (
            <MDBBtn
              size="sm"
              color="light"
              className="btn-block"
              onClick={() =>
                loadUsers(
                  onLoadingUsersSuccess,
                  onLoadingUsersError,
                  state.offset,
                  search.current.value
                )
              }
            >
              Load more...
            </MDBBtn>
          )}

          <MDBBtn className="btn-block" onClick={handleSubmit}>
            Invite
          </MDBBtn>
        </>
      )}
    </div>
  )
}

UsersList.propTypes = {
  members: PropTypes.array.isRequired,
  join: PropTypes.func.isRequired,
}

export default UsersList
