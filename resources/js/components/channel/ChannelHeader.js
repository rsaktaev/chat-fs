import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
} from 'mdbreact'

const ChannelHeader = ({ role, title, toggle, url, user }) => (
  <div className="dialog__header">
    <div>
      <MDBDropdown>
        <MDBDropdownToggle color="white">
          <span className="blue-text">{title}</span>
        </MDBDropdownToggle>
        <MDBDropdownMenu>
          {role ? (
            <>
              <Link to={`${url}/members`}>
                <MDBDropdownItem>Members</MDBDropdownItem>
              </Link>
              <MDBDropdownItem onClick={event => toggle()}>
                Leave
              </MDBDropdownItem>
            </>
          ) : (
            <MDBDropdownItem onClick={event => toggle()}>Join</MDBDropdownItem>
          )}
        </MDBDropdownMenu>
      </MDBDropdown>
    </div>
  </div>
)

ChannelHeader.propTypes = {
  user: PropTypes.object.isRequired,
  role: PropTypes.any.isRequired,
  title: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired,
  url: PropTypes.string.isRequired,
}

export default ChannelHeader
