import React from 'react'
import PropTypes from 'prop-types'
import { MDBAlert, MDBBtn } from 'mdbreact'
import { Route } from 'react-router-dom'
import ChannelHeader from './ChannelHeader'
import Channel from './Channel'
import NewMessage from '../forms/NewMessage'
import ImagesLightbox from '../modals/ImagesLightbox'
import {
  loadMessagesForChannel,
  sendMessageToChannel,
  removeChannel,
  inviteChannel,
  toggleChannel,
} from '../../api'
import Members from './Members'

class ChannelContainer extends React.Component {
  state = {
    channelId: null,
    isLoading: true,
    role: false,
    errorMessage: null,
    sendErrorMessage: null,
    channel: null,
    shouldFetch: false,
    sendingIsLoading: false,
    lightboxIsOpen: false,
    displayIndex: 0,
    imagesList: [],
    typing: '',
  }

  static getDerivedStateFromProps(props, state) {
    const matchId = props.match.params.id

    if (matchId === state.channelId)
      //if haven't changed, state shouldn't be updated
      return {
        channelId: matchId,
        shouldFetch: false,
      }
    else
      return {
        shouldFetch: true,
        channelId: matchId,
      }
  }

  onLoadingMessagesSuccess = response => {
    const responseData = response.data
    if (responseData.error) {
      this.props.onLogout()
    } else {
      this.setState({
        channel: responseData.conversation,
        role: responseData.role,
        errorMessage: null,
        isLoading: false,
        shouldFetch: false,
      })
    }
  }

  onLoadingMessagesError = error => {
    if (error.response && error.response.status === 404) {
      this.props.history.push('/channels')
    }
    this.setState({
      errorMessage: "Can't load the channel!",
      isLoading: false,
      shouldFetch: false,
    })
  }

  loadMessages = () => {
    const { token, match } = this.props
    const id = match.params.id
    const { onLoadingMessagesSuccess, onLoadingMessagesError } = this

    loadMessagesForChannel(
      token,
      id,
      onLoadingMessagesSuccess,
      onLoadingMessagesError
    )
  }

  onSendingMessageSuccess = response => {
    console.log(response)

    if (response.status === 201) {
      this.loadMessages()
      this.setState({ sendErrorMessage: null, sendingIsLoading: false })
    } else {
      this.setState({
        sendErrorMessage: response.data.error || 'Unkown error!',
        sendingIsLoading: false,
      })
    }
  }

  onSendingMessageError = error => {
    const response = error.response
    console.log(response)

    if (response) {
      if (response.data.errors) {
        this.setState({
          sendErrorMessage: response.data.errors.text[0],
          sendingIsLoading: false,
        })
      } else
        this.setState({
          sendErrorMessage:
            response.data.error || "Can't send message. Server error.",
          sendingIsLoading: false,
        })
    } else
      this.setState({
        sendErrorMessage: "Can't send message. Something is wrong.",
        sendingIsLoading: false,
      })
  }

  sendMessage = (text, attachments) => {
    const { user, token } = this.props
    const { onSendingMessageSuccess, onSendingMessageError } = this
    this.setState({ sendingIsLoading: true })

    sendMessageToChannel(
      this.state.channelId,
      user,
      token,
      text,
      attachments,
      onSendingMessageSuccess,
      onSendingMessageError
    )
  }

  onToggleSuccess = response => {
    const responseData = response.data
    if (responseData.success) this.props.loadConversations()
    else
      this.setState({
        errorMessage: responseData,
      })

    return true
  }

  onToggleError = error => {
    console.log(error.response)
    this.setState({
      errorMessage: error.response
        ? 'Server error'
        : 'Error! Something goes wrong.',
    })
  }

  inviteChannel = userId => {
    const { props, state, onToggleSuccess, onToggleError } = this
    const { channelId } = state

    inviteChannel(
      channelId,
      userId,
      props.token,
      onToggleSuccess,
      onToggleError
    ).then(response => {
      if (response) this.loadMessages()
    })
  }

  removeChannel = userId => {
    const { props, state, onToggleSuccess, onToggleError } = this

    removeChannel(
      state.channelId,
      userId,
      props.token,
      onToggleSuccess,
      onToggleError
    ).then(response => {
      if (response) this.loadMessages()
    })
  }

  toggleChannel = () => {
    const { props, state, onToggleSuccess, onToggleError } = this
    toggleChannel(
      state.channelId,
      props.token,
      onToggleSuccess,
      onToggleError
    ).then(response => {
      if (response) this.loadMessages()
    })
  }

  toggleLightbox = (index, imagesList) => {
    this.setState(prevState => ({
      lightboxIsOpen: !prevState.lightboxIsOpen,
      displayIndex: index,
      imagesList: imagesList,
    }))
  }

  render() {
    const {
      isLoading,
      role,
      errorMessage,
      sendErrorMessage,
      channel,
      sendingIsLoading,
      lightboxIsOpen,
      displayIndex,
      imagesList,
      typing,
    } = this.state

    const { user, match, token } = this.props
    const url = match.url
    const { sendMessage, inviteChannel, removeChannel, toggleChannel } = this

    return (
      <React.Fragment>
        {!isLoading ? (
          errorMessage ? (
            <MDBAlert color="danger">{errorMessage}</MDBAlert>
          ) : (
            <div className="dialog__container">
              <ChannelHeader
                user={user}
                title={channel.title}
                toggle={toggleChannel}
                role={role}
                url={url}
              />
              <Channel
                user={user}
                typing={typing}
                messages={channel.messages}
                lightboxIsOpen={lightboxIsOpen}
                toggleLightbox={this.toggleLightbox}
              />

              {role ? (
                <NewMessage
                  user={user}
                  channelpath={`message.${match.params.id}`}
                  isLoading={sendingIsLoading}
                  sendMessage={sendMessage}
                  errorMessage={sendErrorMessage}
                  token={this.props.token}
                />
              ) : (
                <MDBBtn color="primary" onClick={event => toggleChannel()}>
                  Subscribe
                </MDBBtn>
              )}
              {lightboxIsOpen && (
                <ImagesLightbox
                  toggle={this.toggleLightbox}
                  displayIndex={displayIndex}
                  imagesList={imagesList}
                />
              )}

              <Route
                path={`${url}/members`}
                component={props => (
                  <Members
                    {...props}
                    token={token}
                    members={channel.members}
                    prevUrl={url}
                    role={role}
                    join={inviteChannel}
                    leave={removeChannel}
                  />
                )}
              />
            </div>
          )
        ) : (
          <div className="loader__wrapper">
            <div className="spinner-border">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
      </React.Fragment>
    )
  }

  listenChannel = channelId =>
    window.Echo.private(`message.${channelId}`)
      .listen('.send', e => {
        this.setState((state, props) => {
          state.channel.messages.push(e.message)
          return { channel: state.channel }
        })
      })
      .listenForWhisper('typing', e => {
        if (!this.state.typing) {
          this.setState({ typing: e.userName })
          setTimeout(() => this.setState({ typing: '' }), 2000)
        }
      })

  componentDidMount() {
    this.loadMessages()
    this.listenChannel(this.props.match.params.id)
  }

  componentWillUnmount() {
    window.Echo.leave(`message.${this.state.channelId}`)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.shouldFetch) {
      this.loadMessages()
      window.Echo.leave(`message.${prevState.channelId}`)
      this.listenChannel(this.state.id)
    }
  }
}

ChannelContainer.propTypes = {
  user: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
  onLogout: PropTypes.func.isRequired,
  loadConversations: PropTypes.func.isRequired,
}

export default ChannelContainer
