import React from 'react'
import PropTypes from 'prop-types'
import ReactHtmlParser from 'react-html-parser'
import { MDBIcon } from 'mdbreact'
import '../../../css/channel/Channel.css'
import { formattedTime, formatMessage } from '../../utils'

class Channel extends React.Component {
  messageContainer = React.createRef()

  //split array of images and files
  //TODO - вынести функцию!
  filterFiles = attachments => {
    const images = [],
      files = []
    attachments.forEach(element => {
      if (/^image\/.+$/.test(element.mimetype)) {
        images.push(element)
      } else {
        files.push(element)
      }
    })

    return [images, files]
  }

  renderMessages(messages) {
    return messages.map(message => {
      const { id, sender_id, sender, text, attachments } = message

      const formattedText = formatMessage(text) //add links
      const time = new Date(message.created_at)

      const [images, files] = this.filterFiles(attachments)

      if (sender_id === this.props.user.id)
        return (
          <div key={id} className="sendedMessage">
            <div className="message_text">
              {ReactHtmlParser(formattedText)}
              <div className="message__images">
                {images.map((element, index) => (
                  <img
                    key={element.id}
                    src={
                      /^(http|https|ftp):\/\/.*/.test(element.name)
                        ? element.name
                        : `/img/attachments/${element.name}`
                    }
                    alt="attachment"
                    onClick={() => this.props.toggleLightbox(index, images)}
                  />
                ))}
              </div>
              <div>
                {files.map(element => (
                  <div key={element.key}>
                    <a href={`/img/attachments/${element.name}`}>
                      <MDBIcon icon="paperclip" /> {element.name}
                    </a>
                  </div>
                ))}
              </div>
            </div>
            <div className="time">
              <small>{formattedTime(time)}</small>
            </div>
          </div>
        )

      return (
        <div key={id} className="receivedMessage">
          <div className="display_name">
            <small>
              <b>{sender.display_name}</b>
            </small>
          </div>
          <div className="textMessageContainer">
            <img
              src={sender.avatar_url}
              alt="avatar"
              className="rounded-circle avatar"
              width="34"
            />
            {/* message's content */}
            <div className="message_text">
              {ReactHtmlParser(formattedText)}
              <div className="message__images">
                {images.map((element, index) => (
                  <img
                    key={element.id}
                    src={`/img/attachments/${element.name}`}
                    alt="attachment"
                    onClick={() => this.props.toggleLightbox(index, images)}
                  />
                ))}
              </div>
              <div>
                {files.map(element => (
                  <div>
                    <a href={`/img/attachments/${element.name}`}>
                      <MDBIcon icon="paperclip" /> {element.name}
                    </a>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="time">
            <small>{formattedTime(time)}</small>
          </div>
        </div>
      )
    })
  }

  render() {
    const { messages, typing } = this.props
    return (
      <div className="message__wrapper" ref={this.messageContainer}>
        <div className="channel_container">{this.renderMessages(messages)}</div>
        {typing && `${typing} is typing`}
      </div>
    )
  }

  //scroll dialog to the bottom after render
  scrollToBottom = messageContainer => {
    let clientHeight = messageContainer.clientHeight
    let scrollHeight = messageContainer.scrollHeight

    messageContainer.scrollTop = scrollHeight - clientHeight
  }

  componentDidMount() {
    let messageContainer = this.messageContainer.current
    setTimeout(() => this.scrollToBottom(messageContainer), 100)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.lightboxIsOpen === this.props.lightboxIsOpen) {
      let messageContainer = this.messageContainer.current
      setTimeout(() => this.scrollToBottom(messageContainer), 100)
    }
  }
}

Channel.propTypes = {
  user: PropTypes.object.isRequired,
  messages: PropTypes.array.isRequired,
  lightboxIsOpen: PropTypes.bool.isRequired,
  toggleLightbox: PropTypes.func.isRequired,
}

export default Channel
