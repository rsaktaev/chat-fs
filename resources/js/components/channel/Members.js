import React, { useState } from 'react'
import PropTypes from 'prop-types'

import '../../../css/channel/Members.css'
import UsersList from './UsersList'

import { MDBTable, MDBTableBody, MDBBtn } from 'mdbreact'

const Members = props => {
  const closeModal = event => {
    if (event.target.classList.contains('modal_container'))
      props.history.push(props.prevUrl)
  }

  const [showUsers, setShowUsers] = useState(false)

  return (
    <div className="members_container modal_container" onClick={closeModal}>
      <div>
        {showUsers ? (
          <UsersList
            join={props.join}
            members={props.members.map(element => element.id)}
          />
        ) : (
          <>
            <MDBBtn className="btn-block" onClick={event => setShowUsers(true)}>
              Invite...
            </MDBBtn>
            <div className="list">
              <MDBTable>
                <MDBTableBody>
                  {props.members.map(element => (
                    <tr key={element.id}>
                      <td>{element.display_name}</td>
                      <td>
                        {props.role === 'creator' && (
                          <MDBBtn
                            color="danger"
                            size="sm"
                            onClick={event => props.leave([element.id])}
                          >
                            x
                          </MDBBtn>
                        )}
                      </td>
                    </tr>
                  ))}
                </MDBTableBody>
              </MDBTable>
            </div>
          </>
        )}
      </div>
    </div>
  )
}

Members.propTypes = {
  token: PropTypes.string.isRequired,
  members: PropTypes.array.isRequired,
  prevUrl: PropTypes.string.isRequired,
  role: PropTypes.any.isRequired,
  join: PropTypes.func.isRequired,
  leave: PropTypes.func.isRequired,
}

export default Members
