import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'

import PropTypes from 'prop-types'

import '../../../css/pages/HomePage.css'

import SideBar from '../sidebar/SideBar'
import NavHeader from '../header/NavHeader'
import DialogContainer from '../dialog/DialogContainer'
import ChannelContainer from '../channel/ChannelContainer'
import ChannelsList from '../channels/ChannelsList'
import NewChannel from '../forms/NewChannel'
import TasksPage from './TasksPage'

class HomePage extends Component {
  state = {
    isOpened: false,
  }

  /**
   * @param { boolean } close - if true, sidebar always collapsing, otherwise - switches
   */
  collapseSidebar = close =>
    this.setState(state => ({
      isOpened: close ? false : !state.isOpened,
    }))

  render() {
    const {
      user,
      token,
      usersList,
      conversations,
      isLoading,
      onLogout,
      loadConversations,
      setStatus,
      loadUsers,
    } = this.props

    const { isOpened } = this.state

    return (
      <div className="app_wrapper">
        {!isLoading ? (
          <React.Fragment>
            <SideBar
              user={user}
              isOpened={isOpened}
              usersList={usersList}
              conversations={conversations}
              locationPath={this.props.location.pathname}
              collapseSidebar={this.collapseSidebar}
              setStatus={setStatus}
              onLogout={onLogout}
            />
            <div className={`bg${isOpened ? ' opened' : ''}`} />
            <div className="main__content">
              {/* collapse aside, for md  devices */}
              <NavHeader collapseSidebar={this.collapseSidebar} />
              <Switch>
                <Route
                  path="/tasks"
                  render={props =>
                    token && <TasksPage {...props} token={token} user={user} />
                  }
                />
                <Route
                  path="/dialog/:id"
                  render={props => (
                    <DialogContainer
                      {...props}
                      user={user}
                      token={token}
                      onLogout={onLogout}
                      loadUsers={loadUsers}
                    />
                  )}
                />
                <Route
                  path="/channels/add"
                  render={props => (
                    <NewChannel
                      {...props}
                      user={user}
                      token={token}
                      logout={onLogout}
                      loadConversations={loadConversations}
                    />
                  )}
                />
                <Route
                  path="/channels/:id"
                  render={props => (
                    <ChannelContainer
                      {...props}
                      user={user}
                      token={token}
                      onLogout={onLogout}
                      loadConversations={loadConversations}
                    />
                  )}
                />
                <Route
                  path="/channels"
                  render={props => (
                    <ChannelsList
                      {...props}
                      user={user}
                      token={token}
                      onLogout={onLogout}
                    />
                  )}
                />
              </Switch>
            </div>
          </React.Fragment>
        ) : (
          <div className="loader__wrapper">
            <div className="spinner-border">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
      </div>
    )
  }
}

HomePage.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
    display_name: PropTypes.string,
    full_name: PropTypes.string,
    avatar_url: PropTypes.string,
  }),
  token: PropTypes.string.isRequired,
  onLogout: PropTypes.func.isRequired,
  isOpened: PropTypes.bool,
  collapseSidebar: PropTypes.func,
  conversations: PropTypes.shape({
    conversations: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
  }),
  loadUsers: PropTypes.func.isRequired,
  loadConversations: PropTypes.func.isRequired,
  usersList: PropTypes.shape({
    users: PropTypes.array.isRequired,
    size: PropTypes.number.isRequired,
  }).isRequired,
  setStatus: PropTypes.func.isRequired,
}

export default HomePage
