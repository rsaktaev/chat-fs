import React from 'react'
import DialogHeader from '../dialog/DialogHeader'
import { MDBContainer } from 'mdbreact'
import { RoutedTabs, NavTab } from 'react-router-tabs'
import { Switch, Route, Redirect } from 'react-router'
import { AdminRoute } from '../route_components'

import PropTypes from 'prop-types'
import '../../../css/pages/SettingsPage.css'

import WorkspaceSettings from '../settings/WorkspaceSettings'
import EditAccountForm from '../forms/EditAccountForm'

class SettingsPage extends React.Component {
  render = () => {
    const { user, loadUsers, updateUserInfo, token, logout } = this.props

    if (!user) {
      return (
        <div className="loader__wrapper">
          <div className="spinner-border">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )
    }

    const { display_name, avatar_url } = user

    const url = this.props.match.path
    return (
      <div className="settings__page">
        <DialogHeader display_name={display_name} avatar_url={avatar_url} />
        <MDBContainer className="settings__container">
          <RoutedTabs startPathWith={url}>
            {/* link to self account edit form */}
            <NavTab
              className="nav__tab"
              activeClassName="active__nav__tab"
              to={`/account`}
            >
              Account
            </NavTab>
            {/* link to workspace settings (adding, deleting, updating users) */}

            {user.role === 'admin' && (
              <NavTab
                className="nav__tab"
                activeClassName="active__nav__tab"
                to={`/workspace`}
              >
                Workspace
              </NavTab>
            )}
          </RoutedTabs>

          <Switch>
            <Route
              path={`${url}/account`}
              render={props => (
                <EditAccountForm
                  {...props}
                  backUrl="/"
                  user={user}
                  token={token}
                  loggedUser={user}
                  logout={logout}
                  loadUsers={loadUsers}
                  updateUserInfo={updateUserInfo}
                  needsDerivedState={true}
                />
              )}
            />

            <AdminRoute
              path={`${url}/workspace`}
              render={props => (
                <WorkspaceSettings
                  {...props}
                  user={user}
                  token={token}
                  logout={logout}
                  loadUsers={loadUsers}
                  updateUserInfo={updateUserInfo}
                />
              )}
            />

            <Route
              path={url}
              render={() => <Redirect to={`/settings/account`} />}
            />
          </Switch>
        </MDBContainer>
      </div>
    )
  }
}

SettingsPage.propTypes = {
  user: PropTypes.object,
  token: PropTypes.string.isRequired,
  onDeleteUser: PropTypes.func,

  loadUsers: PropTypes.func.isRequired,
  updateUserInfo: PropTypes.func.isRequired,
}

export default SettingsPage
