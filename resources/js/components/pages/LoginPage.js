import React from 'react'
import PropTypes from 'prop-types'
import { MDBContainer, MDBRow, MDBCol, MDBAlert } from 'mdbreact'

import LoginForm from '../forms/LoginForm'

const LoginPage = ({ errorMessage, onLogin, history, isLoading, errors }) => (
  <MDBContainer>
    <MDBRow>
      {isLoading ? (
        <div className="loader__wrapper">
          <div className="spinner-border">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      ) : (
        <MDBCol sm="10" md="6" className="offset-sm-1 offset-md-3 mt-5">
          <h1>Sign in</h1>
          {errorMessage && <MDBAlert color="danger">{errorMessage}</MDBAlert>}
          <LoginForm
            history={history}
            isLoading={isLoading}
            onLogin={onLogin}
            errors={errors}
          />
        </MDBCol>
      )}
    </MDBRow>
  </MDBContainer>
)

LoginPage.propTypes = {
  onLogin: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  errors: PropTypes.object.isRequired,
}

export default LoginPage
