import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import DialogHeader from '../dialog/DialogHeader'
import TasksList from '../tasks/TasksList'
import NewTask from '../forms/NewTask'
import { loadTasks } from '../../api'

import { MDBContainer, MDBAlert, MDBBtn, MDBCollapse } from 'mdbreact'

const reducer = (state, action) => {
  switch (action.type) {
    case 'LOADING':
      return { ...state, isLoading: !state.isLoading }
    case 'TASKS_LOADING_SUCCESS':
      return {
        ...state,
        tasks: action.payload,
        errorMessage: '',
        isLoading: false,
      }
    case 'TASKS_LOADING_ERROR':
      return {
        ...state,
        tasks: [],
        errorMessage: action.payload,
        isLoading: false,
      }
    default:
      return state
  }
}

const TasksPage = ({ user, token }) => {
  const [state, dispatch] = useReducer(reducer, {
    tasks: [],
    errorMessage: '',
    isLoading: false,
  })

  const [collapseId, setCollapseId] = useState('')

  const _onLoadingTasksSuccess = response => {
    if (response.data) {
      dispatch({ type: 'TASKS_LOADING_SUCCESS', payload: response.data })
    } else {
      dispatch({
        type: 'TASKS_LOADING_ERROR',
        payload: 'Tasks hasnt been loaded!',
      })
    }
  }

  const _onLoadingTasksError = error => {
    dispatch({
      type: 'TASKS_LOADING_ERROR',
      payload: 'Tasks hasnt been loaded!',
    })
  }

  const _loadTasks = () => {
    dispatch({ type: 'LOADING' })
    loadTasks(token, _onLoadingTasksSuccess, _onLoadingTasksError)
  }

  useEffect(() => {
    _loadTasks()
  }, [])

  const toggleCollapse = collapseId =>
    setCollapseId(prevCollapseId =>
      prevCollapseId === collapseId ? '' : collapseId
    )

  const { display_name, avatar_url, role } = user
  const { isLoading, errorMessage, tasks } = state

  const header =
    role === 'admin' ? (
      <>
        <h1 className="mt-4">Tasks created by you</h1>

        <MDBBtn
          block
          color="primary"
          className="mb-2"
          onClick={() => toggleCollapse('createForm')}
        >
          Create a new task
        </MDBBtn>
        <MDBCollapse id="createForm" isOpen={collapseId}>
          <NewTask {...{ token, user, loadTasks: _loadTasks }} />
        </MDBCollapse>
      </>
    ) : (
      <h1 className="mt-4">Tasks assigned to you</h1>
    )
  return user ? (
    <div className="tasks__page">
      <DialogHeader display_name={display_name} avatar_url={avatar_url} />
      <MDBContainer>
        {header}
        {errorMessage ? (
          <MDBAlert color="danger">{errorMessage}</MDBAlert>
        ) : !isLoading ? (
          <TasksList tasks={tasks} token={token} />
        ) : (
          <div className="loader__wrapper">
            <div className="spinner-border">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
      </MDBContainer>
    </div>
  ) : (
    <div className="loader__wrapper">
      <div className="spinner-border">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  )
}

TasksPage.propTypes = {
  user: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
}
export default TasksPage
