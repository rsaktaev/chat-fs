import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'

import UsersList from './UsersList'

import AddAccountForm from '../forms/AddAccountForm'
import EditAccountForm from '../forms/EditAccountForm'

class WorkspaceSettings extends React.Component {
  render() {
    const url = this.props.match.path
    const { user, logout, loadUsers, updateUserInfo, token } = this.props

    return (
      <Switch>
        <Route
          path={`${url}`}
          exact
          component={props => <UsersList {...props} token={token} />}
        />

        <Route
          path={`${url}/add`}
          render={props => (
            <AddAccountForm
              {...props}
              user={user}
              token={token}
              logout={logout}
              loadUsers={loadUsers}
            />
          )}
        />
        <Route
          path={`${url}/:id/edit`}
          render={props => (
            <EditAccountForm
              {...props}
              loggedUser={user}
              token={token}
              logout={logout}
              backUrl="/settings/workspace"
              loadUsers={loadUsers}
              needsDerivedState={false}
              updateUserInfo={updateUserInfo}
            />
          )}
        />
        <Route path={`${url}/*`} component={props => <div>404</div>} />
      </Switch>
    )
  }
}

WorkspaceSettings.propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,

  loadUsers: PropTypes.func.isRequired,
  updateUserInfo: PropTypes.func.isRequired,
}

export default WorkspaceSettings
