import React from 'react'
import PropTypes from 'prop-types'

import {
  MDBListGroup,
  MDBListGroupItem,
  MDBBtn,
  MDBAlert,
  MDBIcon,
} from 'mdbreact'
import { Link } from 'react-router-dom'
import { loadUsers } from '../../api'
import { OFFSET } from '../../constants'

class UsersList extends React.Component {
  state = {
    isLoading: true,
    errorMessage: null,
    users: { users: [], size: 0 },
    offset: 0,
  }

  search = React.createRef()

  renderUsersList = users =>
    users.users.map(element => {
      const path = this.props.match.path

      return (
        <Link key={element.id} to={`${path}/${element.id}/edit`}>
          <MDBListGroupItem hover>{element.display_name}</MDBListGroupItem>
        </Link>
      )
    })

  onSearch = event => {
    const search = event.target.value
    loadUsers(this.onSearchSuccess, this.onLoadingUsersError, 0, search)
  }

  onSearchSuccess = response => {
    const data = response.data

    if (data) {
      this.setState(prevState => ({
        users: {
          users: [...data.users],
          size: data.size,
        },
        offset: OFFSET,
        isLoading: false,
      }))
    } else {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    }
  }

  render() {
    const url = this.props.match.path
    const { users, errorMessage, offset } = this.state
    console.log(users.size)
    return (
      <React.Fragment>
        <h1 className="mt-5 mb-3">Users in workspace</h1>
        <Link to={`${url}/add`}>
          <MDBBtn className="btn-block mb-3" color="primary">
            Add new user
          </MDBBtn>
        </Link>
        <div className="form-group input-group">
          <input
            className="form-control"
            ref={this.search}
            onChange={this.onSearch}
          />
          <div className="input-group-append">
            <div className="input-group-text">
              <MDBIcon icon="search" />
            </div>
          </div>
        </div>
        {errorMessage ? (
          <MDBAlert color="danger">{errorMessage}</MDBAlert>
        ) : (
          <>
            <MDBListGroup>{this.renderUsersList(users)}</MDBListGroup>
            {users.size > offset && (
              <MDBBtn
                color="light"
                size="sm"
                className="btn-block mt-1"
                onClick={() => this.loadUsers(offset)}
              >
                Load more...
              </MDBBtn>
            )}
          </>
        )}
      </React.Fragment>
    )
  }

  onLoadingUsersSuccess = response => {
    const data = response.data
    if (data) {
      this.setState(prevState => ({
        users: {
          users: [...prevState.users.users, ...data.users],
          size: data.size,
        },
        isLoading: false,
        offset: prevState.offset + OFFSET,
      }))
    } else {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    }
  }

  onLoadingUsersError = error => {
    if (error.response) {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    } else {
      this.setState({
        errorMessage: 'Server error.',
        isLoading: false,
      })
    }
  }

  /**
   * Async method loads channels list from server
   *
   */
  loadUsers = offset =>
    loadUsers(
      this.onLoadingUsersSuccess,
      this.onLoadingUsersError,
      offset,
      this.search.current.value
    )

  componentDidMount() {
    this.loadUsers()
  }
}

UsersList.propTypes = {
  token: PropTypes.string.isRequired,
}

export default UsersList
