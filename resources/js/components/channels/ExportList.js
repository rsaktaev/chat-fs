import React from 'react'
import PropTypes from 'prop-types'
import {
  MDBListGroup,
  MDBListGroupItem,
  MDBAlert,
  MDBContainer,
  MDBBtn,
  MDBIcon,
} from 'mdbreact'
import { loadChannels, createSlackChannel } from '../../api'
import { OFFSET } from '../../constants'
import swal from 'sweetalert'

class ExportList extends React.Component {
  state = {
    exportChannels: new Set(),
    isLoading: false,
    errorMessage: null,
    channels: [],
    size: 0,
    offset: 0,
    exportErrorMessage: '',
    exportSuccessMessage: '',
    exportErrors: 0,
  }

  search = React.createRef()

  onChannelSelect = event => {
    const listItem = event.target

    this.setState(prevState => {
      const currentChannels = new Set(prevState.exportChannels)
      const value = parseInt(listItem.dataset.value)
      if (!listItem.classList.contains('active')) currentChannels.add(value)
      else currentChannels.delete(value)

      if (currentChannels.size >= 10) {
        swal({
          icon: 'error',
          title: 'Max length of channels can be imported: 10',
        })
        return {}
      }

      return { exportChannels: currentChannels }
    })
  }

  renderChannelsList = channels =>
    channels.map(element => (
      <MDBListGroupItem
        hover
        onClick={this.onChannelSelect}
        key={element.id}
        data-value={element.id}
        className={`form-check${
          this.state.exportChannels.has(element.id) ? ' active' : ''
        }`}
      >
        {element.title}
      </MDBListGroupItem>
    ))

  onSearch = event => {
    const search = event.target.value
    loadChannels(
      this.props.token,
      this.onSearchSuccess,
      this.onLoadingUsersError,
      0,
      search
    )
  }

  onSearchSuccess = response => {
    const data = response.data

    if (data) {
      this.setState({
        channels: [...data.channels],
        size: data.size,
        offset: OFFSET,
        isLoading: false,
      })
    } else {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    }
  }

  onLoadingChannelsSuccess = response => {
    const data = response.data
    if (data.error) {
      this.props.onLogout()
    } else {
      this.setState(prevState => ({
        channels: [...prevState.channels, ...data.channels],
        size: data.size,
        offset: prevState.offset + OFFSET,
        isLoading: false,
      }))
    }
  }

  onLoadingChannelsError = error => {
    if (error.response) {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    } else {
      this.setState({
        errorMessage: 'Server error.',
        isLoading: false,
      })
    }
  }

  /**
   * Async method loads channels list from server
   *
   */
  loadChannels = () => {
    this.setState({ isLoading: true })
    loadChannels(
      this.props.token,
      this.onLoadingChannelsSuccess,
      this.onLoadingChannelsError,
      this.state.offset,
      this.search.current.value
    )
  }

  onSlackExportSuccess = response => {
    if (response.data) {
      if (response.data.ok)
        this.setState({
          exportSuccessMessage: 'All channels has been exported',
        })
      else
        this.setState(prevState => ({
          exportSuccessMessage: '',
          exportErrors: prevState.exportErrors + 1,
        }))
    } else {
      this.setState(prevState => ({
        exportSuccessMessage: '',
        exportErrors: prevState.exportErrors + 1,
      }))
    }
  }

  onSlackExportError = error => {
    const response = error.response

    if (!response) {
      this.setState(prevState => ({
        exportSuccessMessage: '',
        exportErrors: prevState.exportErrors + 1,
      }))
    } else if (response.status === 401) {
      if (response.data.redirect) {
        swal({
          icon: 'error',
          title: 'Access denied',
          text: 'Do you want to Slack authorization?',
          button: 'Authorize to Slack',
        }).then(value => {
          if (value) window.location.replace(response.data.url)
        })
      } else {
        this.props.onLogout()
      }
    } else {
      this.setState(prevState => ({
        exportSuccessMessage: '',
        exportErrors: prevState.exportErrors + 1,
      }))
    }
  }

  onExport = event => {
    event.preventDefault()

    this.setState({
      exportSuccessMessage: '',
      exportErrors: 0,
      isLoading: true,
      exportErrorMessage: '',
    })

    const fetches = Array.from(this.state.exportChannels).map(element =>
      createSlackChannel(
        element,
        this.props.token,
        this.onSlackExportSuccess,
        this.onSlackExportError
      )
    )

    Promise.all(fetches).then(res =>
      this.setState({ isLoading: false, exportChannels: new Set() })
    )
  }

  handleToggle = event => {
    if (event.target.classList.contains('modal_container')) return
    this.props.toggleModal()
  }

  render() {
    const {
      channels,
      errorMessage,
      offset,
      size,
      isLoading,
      exportErrorMessage,
      exportSuccessMessage,
      exportErrors,
    } = this.state

    return (
      <div
        className="members_container modal_container"
        onClick={this.props.toggleModal}
      >
        <MDBIcon
          id="close_btn"
          className="dialog_lightbox__button close_btn text-light"
          icon="times"
        />
        <MDBContainer onClick={this.handleToggle}>
          <h1 className="mt-5 mb-3">Channels to export</h1>

          {exportErrorMessage && (
            <MDBAlert color="danger">{exportErrorMessage}</MDBAlert>
          )}
          {/* quantity of non exported channels or success alert */}
          {exportErrors > 0 ? (
            <MDBAlert color="danger">
              Quantity of non exported channels: {exportErrors}
            </MDBAlert>
          ) : (
            exportSuccessMessage && (
              <MDBAlert color="success">{exportSuccessMessage}</MDBAlert>
            )
          )}

          <div className="form-group input-group">
            <input
              className="form-control"
              ref={this.search}
              onChange={this.onSearch}
            />
            <div className="input-group-append">
              <div className="input-group-text">
                <MDBIcon icon="search" />
              </div>
            </div>
          </div>
          <div className="list">
            <MDBListGroup>
              {errorMessage ? (
                <MDBAlert color="danger">{errorMessage}</MDBAlert>
              ) : (
                this.renderChannelsList(channels)
              )}
            </MDBListGroup>
          </div>
          {offset < size && (
            <MDBBtn
              color="light"
              className="btn-block mt-1"
              size="sm"
              onClick={event => this.loadChannels(offset)}
            >
              Load more...
            </MDBBtn>
          )}

          <MDBBtn
            outline
            color="primary"
            disabled={isLoading}
            onClick={this.onExport}
            size="md"
            className="btn-block mt-4 mb-3"
          >
            Export
          </MDBBtn>
        </MDBContainer>
      </div>
    )
  }

  componentDidMount() {
    this.loadChannels()
  }
}

ExportList.propTypes = {
  token: PropTypes.string.isRequired,
  toggleModal: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
}

export default ExportList
