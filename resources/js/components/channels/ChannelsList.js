import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import {
  MDBListGroup,
  MDBListGroupItem,
  MDBAlert,
  MDBContainer,
  MDBBtn,
  MDBIcon,
} from 'mdbreact'
import { loadChannels, slackChannels, createChannel } from '../../api'
import { OFFSET } from '../../constants'
import swal from 'sweetalert'
import ExportList from './ExportList'

class ChannelsList extends React.Component {
  state = {
    isLoading: false,
    errorMessage: null,
    channels: [],
    size: 0,
    offset: 0,
    exportSuccessMessage: '',
    exportErrorMessage: '',
    exportErrors: 0,
    importSuccessMessage: '',
    importErrorMessage: '',
    importErrors: 0,
    isExportListOpened: false,
  }

  search = React.createRef()

  renderChannelsList = channels =>
    channels.map(element => (
      <Link key={element.id} to={`${this.props.match.url}/${element.id}`}>
        <MDBListGroupItem hover>{element.title}</MDBListGroupItem>
      </Link>
    ))

  onSearch = event => {
    const search = event.target.value
    loadChannels(
      this.props.token,
      this.onSearchSuccess,
      this.onLoadingUsersError,
      0,
      search
    )
  }

  onSearchSuccess = response => {
    const data = response.data

    if (data) {
      this.setState({
        channels: [...data.channels],
        size: data.size,
        offset: OFFSET,
        isLoading: false,
      })
    } else {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    }
  }

  onLoadingSlackChannelsSuccess = response => {
    const responseData = response.data
    if (!responseData) {
      //TODO- error
      this.setState({
        importSuccessMessage: '',
        importErrorMessage: 'Loading slack channels has fallen',
      })
    } else {
      if (responseData.ok) {
        const { user, token } = this.props
        responseData.channels.forEach(element =>
          createChannel(
            user.id,
            element.name,
            [],
            token,
            this.onImportSuccess,
            this.onImportError
          )
        )
        const cursor = responseData.response_metadata.next_cursor
        if (cursor) this.slackImport(cursor)
      } else {
        this.setState({
          importSuccessMessage: '',
          importErrorMessage: 'Loading slack channels has fallen',
        })
      }
    }
  }

  onLoadingSlackChannelsError = error => {
    const response = error.response
    console.log(response)

    if (!response) {
      this.setState({
        importSuccessMessage: '',
        importErrorMessage: 'Loading slack channels has fallen',
      })
    } else if (response.status === 401) {
      if (response.data.redirect) {
        swal({
          icon: 'error',
          title: 'Access denied',
          text: 'Do you want to Slack authorization?',
          button: 'Authorize to Slack',
        }).then(value => {
          if (value) window.location.replace(response.data.url)
        })
      } else {
        this.props.onLogout()
      }
    } else {
      this.setState({
        importSuccessMessage: '',
        importErrorMessage: 'Loading slack channels has fallen',
      })
    }
  }

  onImportSuccess = response => {
    if (response.data) {
      this.setState(prevState => ({
        importSuccessMessage: 'All channels has been imported',
      }))
    } else {
      this.setState(prevState => ({
        importSuccessMessage: '',
        importErrors: prevState.importErrors + 1,
      }))
    }
  }

  onImportError = error => {
    this.setState(prevState => ({
      importSuccessMessage: '',
      importErrors: prevState.importErrors + 1,
    }))
  }

  onImport = event => {
    this.setState({
      importErrorMessage: '',
      importSuccessMessage: '',
      importErrors: 0,
    })
    this.slackImport()
  }

  slackImport = next =>
    slackChannels(
      this.props.token,
      this.onLoadingSlackChannelsSuccess,
      this.onLoadingSlackChannelsError,
      next
    )

  onLoadingChannelsSuccess = response => {
    const data = response.data
    if (data.error) {
      this.props.onLogout()
    } else {
      this.setState(prevState => ({
        channels: [...prevState.channels, ...data.channels],
        size: data.size,
        offset: prevState.offset + OFFSET,
        isLoading: false,
      }))
    }
  }

  onLoadingChannelsError = error => {
    if (error.response) {
      this.setState({
        errorMessage: 'Error!',
        isLoading: false,
      })
    } else {
      this.setState({
        errorMessage: 'Server error.',
        isLoading: false,
      })
    }
  }

  /**
   * Async method loads channels list from server
   *
   */
  loadChannels = () => {
    this.setState({ isLoading: true })
    loadChannels(
      this.props.token,
      this.onLoadingChannelsSuccess,
      this.onLoadingChannelsError,
      this.state.offset,
      this.search.current.value
    )
  }

  toggleModal = () =>
    this.setState(prevState => ({
      isExportListOpened: !prevState.isExportListOpened,
    }))

  render() {
    const {
      channels,
      errorMessage,
      offset,
      size,
      isLoading,
      importSuccessMessage,
      importErrorMessage,
      importErrors,
      isExportListOpened,
    } = this.state

    return (
      <MDBContainer className="pb-4">
        <h1 className="mt-5 mb-3">Channels</h1>
        <Link to={`${this.props.match.url}/add`}>
          <MDBBtn color="primary" className="btn-block">
            New channel
          </MDBBtn>
        </Link>

        <div className="btn-group w-100">
          <MDBBtn
            outline
            color="primary"
            disabled={isLoading}
            onClick={this.toggleModal}
            size="md"
            className="btn-block mb-3"
          >
            Export channels to{' '}
            <span style={{ color: 'black' }}>
              Slack <MDBIcon fab icon="slack-hash" />
            </span>
          </MDBBtn>
          <MDBBtn
            outline
            color="primary"
            size="md"
            disabled={isLoading}
            onClick={this.onImport}
            className="btn-block mb-3"
          >
            Import channels from{' '}
            <span style={{ color: 'black' }}>
              Slack <MDBIcon fab icon="slack-hash" />
            </span>
          </MDBBtn>
        </div>

        {/* if some channels hasnt been loaded */}
        {importErrorMessage && (
          <MDBAlert color="danger">{importErrorMessage}</MDBAlert>
        )}
        {/* quantity of non imported channels or success alert */}
        {importErrors > 0 ? (
          <MDBAlert color="danger">
            Quantity of non imported channels: {importErrors}
          </MDBAlert>
        ) : (
          importSuccessMessage && (
            <MDBAlert color="success">{importSuccessMessage}</MDBAlert>
          )
        )}
        <div className="form-group input-group">
          <input
            className="form-control"
            ref={this.search}
            onChange={this.onSearch}
          />
          <div className="input-group-append">
            <div className="input-group-text">
              <MDBIcon icon="search" />
            </div>
          </div>
        </div>
        <MDBListGroup className="mb-1">
          {errorMessage ? (
            <MDBAlert color="danger">{errorMessage}</MDBAlert>
          ) : (
            this.renderChannelsList(channels)
          )}
        </MDBListGroup>

        {offset < size && (
          <MDBBtn
            color="light"
            className="btn-block"
            size="sm"
            onClick={event => this.loadChannels(offset)}
          >
            Load more...
          </MDBBtn>
        )}

        {isExportListOpened && (
          <ExportList
            token={this.props.token}
            onLogout={this.props.onLogout}
            toggleModal={this.toggleModal}
          />
        )}
      </MDBContainer>
    )
  }

  componentDidMount() {
    this.loadChannels()
  }
}

ChannelsList.propTypes = {
  user: PropTypes.object.isRequired,
  onLogout: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
}

export default ChannelsList
