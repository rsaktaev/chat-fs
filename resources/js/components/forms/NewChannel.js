import React, { useState } from 'react'
import '../../../css/forms/NewChannel.css'
import { MDBInput, MDBContainer, MDBBtn, MDBAlert } from 'mdbreact'
import { createChannel } from '../../api'
import PropTypes from 'prop-types'
import { CREATE_SUCCESS, CREATE_ERROR } from '../../constants'
import UsersList from './UsersList'

const NewChannel = React.memo(props => {
  const { user, token, logout, loadConversations } = props

  const [title, setTitle] = useState('')

  const [creatingStatus, setCreatingStatus] = useState({
    alert: '',
    status: '',
    errors: {},
  })

  const [disabled, setDisabled] = useState(false)
  const [members, setMembers] = useState(new Set())

  const _onCreatingChannelSuccess = response => {
    const responseData = response.data
    setDisabled(false)

    if (responseData) {
      loadConversations()
      setCreatingStatus(prevState => ({
        ...prevState,
        errors: {},
        alert: `New channel "${responseData.title}" has been created`,
        status: CREATE_SUCCESS,
      }))
    } else {
      setCreatingStatus(prevState => ({
        ...prevState,
        errors: {},
        alert: 'Something is wrong!',
        status: CREATE_ERROR,
      }))
    }
  }

  const _onCreatingChannelError = error => {
    const response = error.response
    setDisabled(false)

    console.log(response)

    if (response && response.status === 401) {
      logout()
    } else if (response.data && response.data.errors) {
      setCreatingStatus(prevState => ({
        ...prevState,
        errors: response.data.errors,
        alert: '',
        status: '',
      }))
    } else
      setCreatingStatus(prevState => ({
        ...prevState,
        errors: {},
        alert: "Can't create new channel!",
        status: CREATE_ERROR,
      }))
  }

  // toggle users in the set
  const onUserSelect = event => {
    const listItem = event.target

    setMembers(prevState => {
      prevState = new Set(prevState)
      const value = parseInt(listItem.dataset.value)
      if (!listItem.classList.contains('active')) prevState.add(value)
      else prevState.delete(value)

      return prevState
    })
  }

  const handleSubmit = event => {
    event.preventDefault()
    setDisabled(true)

    createChannel(
      user.id,
      title,
      [...members],
      token,
      _onCreatingChannelSuccess,
      _onCreatingChannelError
    )
  }

  const { alert, status, errors } = creatingStatus

  return (
    <MDBContainer className="pb-4">
      <h1 className="mt-5 mb-3">New Channel</h1>
      {status === CREATE_ERROR && <MDBAlert color="danger">{alert}</MDBAlert>}
      {status === CREATE_SUCCESS ? (
        <MDBAlert color="success">{alert}</MDBAlert>
      ) : (
        <form action="" id="newChannelForm" onSubmit={handleSubmit}>
          <div className="form-group">
            <MDBInput
              name="title"
              value={title}
              label="New channel's title"
              onChange={event => setTitle(event.target.value)}
            />
            {errors.title && (
              <div className="invalid__feedback">{errors.title[0]}</div>
            )}
          </div>

          <UsersList {...{ onUserSelect, members }} title="Members: " />

          <MDBBtn
            color="success"
            type="submit"
            disabled={disabled}
            className="btn-block"
          >
            Create new channel
          </MDBBtn>
        </form>
      )}
    </MDBContainer>
  )
})

NewChannel.propTypes = {
  token: PropTypes.string.isRequired,
}

export default NewChannel
