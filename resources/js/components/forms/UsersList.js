import React, { useState, useEffect, useRef } from 'react'
import '../../../css/forms/NewChannel.css'
import { MDBListGroupItem, MDBListGroup, MDBBtn, MDBIcon } from 'mdbreact'
import { loadUsers } from '../../api'
import PropTypes from 'prop-types'
import { OFFSET } from '../../constants'

const UsersList = React.memo(props => {
  const { title, onUserSelect, members } = props

  const [users, setUsers] = useState([])
  const [size, setSize] = useState(0)
  const [offset, setOffset] = useState(0)
  const search = useRef()

  const _onSearchSuccess = response => {
    const data = response.data
    if (data) {
      setUsers(data.users)
      setSize(data.size)
      setOffset(OFFSET)
    } else {
      setUsers([])
      setSize(0)
      setOffset(0)
    }
  }

  const _onSuccess = response => {
    const data = response.data
    if (data) {
      setUsers(users => [...users, ...data.users])
      setSize(data.size)
      setOffset(offset => offset + OFFSET)
    } else {
      setUsers([])
      setSize(0)
      setOffset(0)
    }
  }

  const _onError = error => {
    setUsers([])
    setSize(0)
    setOffset(0)
  }

  useEffect(() => {
    loadUsers(_onSuccess, _onError)
  }, [])

  const onSearch = event => {
    const search = event.target.value
    loadUsers(_onSearchSuccess, _onError, 0, search)
  }

  return (
    <div className="form-group">
      <label htmlFor="channelMembers">{title}</label>
      <div className="form-group input-group">
        <input ref={search} className="form-control" onChange={onSearch} />
        <div className="input-group-append">
          <div className="input-group-text">
            <MDBIcon icon="search" />
          </div>
        </div>
      </div>
      <MDBListGroup>
        {users.map(element => (
          <MDBListGroupItem
            hover
            onClick={onUserSelect}
            key={element.id}
            data-value={element.id}
            className={
              'form-check' + (members.has(element.id) ? ' active' : '')
            }
          >
            <label className="form-check-label">{element.display_name}</label>
          </MDBListGroupItem>
        ))}
      </MDBListGroup>
      {size > offset && (
        <MDBBtn
          color="light"
          className="btn btn-sm btn-block mt-1"
          onClick={() =>
            loadUsers(_onSuccess, _onError, offset, search.current.value)
          }
        >
          load more...
        </MDBBtn>
      )}
    </div>
  )
})

UsersList.propTypes = {
  title: PropTypes.string.isRequired,
  onUserSelect: PropTypes.func.isRequired,
  members: PropTypes.any.isRequired,
}

export default UsersList
