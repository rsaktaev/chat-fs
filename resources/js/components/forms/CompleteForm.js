import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { MDBInput, MDBBtn, MDBAlert } from 'mdbreact'
import AttachmentDropzone from './AttachmentDropzone'
import { completeTask } from '../../api'
const CompleteForm = ({ taskId, manuallyRedraw, setTask, token }) => {
  const [state, setState] = useState({
    attachments: [],
    comment: '',
    errors: {},
    errorMessage: '',
  })

  const _onSuccess = response => {
    const responseData = response.data
    if (responseData.success) {
      setState(prevState => ({ ...prevState, errors: {}, errorMessage: '' }))
      setTask(responseData.task)
    } else {
      setState(prevState => ({
        ...prevState,
        errors: {},
        errorMessage: 'Something is wrong',
      }))
    }
  }

  const _onError = error => {
    const response = error.response
    if (response && response.data) {
      if (response.data.errors) {
        setState(prevState => ({
          ...prevState,
          errors: response.data.errors,
          errorMessage: '',
        }))
      } else {
        setState(prevState => ({
          ...prevState,
          errors: {},
          errorMessage: response.data.message || 'Something is wrong',
        }))
      }
    } else {
      setState(prevState => ({
        ...prevState,
        errors: {},
        errorMessage: 'Something is wrong',
      }))
    }
  }

  const handleSubmit = () =>
    completeTask(
      token,
      taskId,
      state.comment,
      state.attachments,
      _onSuccess,
      _onError
    )

  const { attachments, comment, errors, errorMessage } = state
  return (
    <div>
      <form action="">
        {errorMessage && <MDBAlert color="danger">{errorMessage}</MDBAlert>}
        <div className="form-group">
          <AttachmentDropzone
            attachments={attachments}
            addAttachment={file =>
              setState(prevState => ({
                ...prevState,
                attachments: [...prevState.attachments, file],
              }))
            }
            dropAttachment={id =>
              setState(prevState => ({
                ...prevState,
                attachments: prevState.attachments.filter(el => el.id !== id),
              }))
            }
            token={token}
            manuallyRedraw={manuallyRedraw}
          />
          {errors.attachments && (
            <div className="invalid__feedback">{errors.attachments[0]}</div>
          )}
        </div>
        <div className="form-group">
          <MDBInput
            type="text"
            value={comment}
            onChange={event => {
              const newVal = event.target.value
              setState(prevState => ({
                ...prevState,
                comment: newVal,
              }))
            }}
            label="Comment"
          />
          {errors.description && (
            <div className="invalid__feedback">{errors.description[0]}</div>
          )}
        </div>
        <div className="form-group">
          <MDBBtn block color="success" onClick={handleSubmit}>
            Complete!
          </MDBBtn>
        </div>
      </form>
    </div>
  )
}

CompleteForm.propTypes = {
  taskId: PropTypes.number.isRequired,
  manuallyRedraw: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  setTask: PropTypes.func.isRequired,
}

export default CompleteForm
