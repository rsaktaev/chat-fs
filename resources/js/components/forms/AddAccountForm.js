import React from 'react'
import PropTypes from 'prop-types'

import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBInput,
  MDBBtn,
  MDBIcon,
  MDBAlert,
} from 'mdbreact'

import { CREATE_SUCCESS, CREATE_ERROR } from '../../constants'

import { Link } from 'react-router-dom'
import { createUser } from '../../api'

class AddAccountForm extends React.Component {
  state = {
    display_name: '',
    full_name: '',
    email: '',
    phone: '',
    status: '',
    alertMessage: '',
    errors: {},
    needsDerivedState: true,
    isLoading: false,
  }

  onCreatingUserSuccess = response => {
    if (!response.data) {
      this.setState({
        status: CREATE_ERROR,
        alertMessage: "Can't create a new account",
        isLoading: false,
      })
    } else {
      this.props.loadUsers()
      this.setState({
        errors: {},
        status: CREATE_SUCCESS,
        alertMessage: 'User has been created successfully.',
        isLoading: false,
      })
    }
  }

  onCreatingUserError = error => {
    let response = error.response

    if (response && response.status === 401) {
      //if validation fails
      this.props.logout()
    } else {
      if (response.data.errors) {
        this.setState({
          status: '',
          errors: response.data.errors,
          isLoading: false,
        })
      } else {
        //some errors on server
        this.setState({
          status: CREATE_ERROR,
          alertMessage: 'Server error',
          isLoading: false,
        })
      }
    }
  }

  /**
   * Async request to create user
   *
   * @param {object} event
   */
  handleSubmit = event => {
    event.preventDefault()
    this.setState({ isLoading: true })
    const { display_name, full_name, email, phone } = this.state
    createUser(
      { display_name, full_name, email, phone },
      this.props.token,
      this.onCreatingUserSuccess,
      this.onCreatingUserError
    )
  }

  onInputChange = event => {
    const field = event.currentTarget.name
    const value = event.currentTarget.value
    this.setState({ [field]: value })
  }

  render() {
    const {
      display_name,
      full_name,
      email,
      phone,
      alertMessage,
      errors,
      status,
      isLoading,
    } = this.state

    return (
      <MDBContainer className="settings__subcontainer">
        <Link to="/settings/workspace">
          <MDBIcon icon="arrow-left" size="2x" />
        </Link>
        {status === CREATE_ERROR && (
          <MDBAlert color="danger">{alertMessage}</MDBAlert>
        )}
        {status === CREATE_SUCCESS ? (
          <MDBAlert color="success">{alertMessage}</MDBAlert>
        ) : (
          <form>
            <MDBRow>
              <MDBCol size="6" md="3" className="offset-md-0 offset-3">
                <MDBContainer>
                  <div className="rounded-circle">
                    <img
                      className="rounded-circle img-fluid"
                      src="https://ssl.gstatic.com/images/branding/product/1x/avatar_square_blue_512dp.png"
                      alt="user avatar"
                    />
                  </div>
                </MDBContainer>
              </MDBCol>
              <MDBCol md="9">
                <MDBContainer>
                  <MDBInput
                    type="text"
                    name="display_name"
                    value={display_name}
                    onChange={this.onInputChange}
                    icon="male"
                    label="Display name"
                  />
                  {errors.display_name && (
                    <div className="invalid__feedback">
                      {errors.display_name[0]}
                    </div>
                  )}
                  <MDBInput
                    type="text"
                    name="full_name"
                    value={full_name}
                    onChange={this.onInputChange}
                    icon="id-card"
                    label="Full name"
                  />
                  {errors.full_name && (
                    <div className="invalid__feedback">
                      {errors.full_name[0]}
                    </div>
                  )}
                  <MDBInput
                    type="text"
                    name="email"
                    value={email}
                    onChange={this.onInputChange}
                    icon="envelope"
                    label="Email"
                  />
                  {errors.email && (
                    <div className="invalid__feedback">{errors.email[0]}</div>
                  )}
                  <MDBInput
                    type="text"
                    name="phone"
                    value={phone}
                    onChange={this.onInputChange}
                    icon="mobile"
                    label="Phone number"
                  />
                  {errors.phone && (
                    <div className="invalid__feedback">{errors.phone[0]}</div>
                  )}
                </MDBContainer>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol size="12">
                <MDBContainer>
                  <MDBBtn
                    type="submit"
                    color="primary"
                    block
                    disabled={isLoading}
                    onClick={this.handleSubmit}
                  >
                    Submit
                  </MDBBtn>
                </MDBContainer>
              </MDBCol>
            </MDBRow>
          </form>
        )}
      </MDBContainer>
    )
  }
}

AddAccountForm.propTypes = {
  user: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
  loadUsers: PropTypes.func.isRequired,
}

export default AddAccountForm
