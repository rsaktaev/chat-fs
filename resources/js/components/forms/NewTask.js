import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { MDBInput, MDBBtn, MDBAlert } from 'mdbreact'
import AttachmentDropzone from './AttachmentDropzone'
import UsersList from './UsersList'
import { createTask } from '../../api'

const NewTask = React.memo(({ token, user, loadTasks }) => {
  console.log('KEK')

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')

  const [executors, setExecutors] = useState(new Set())
  const [attachment, setAttachment] = useState('')
  const [status, setStatus] = useState({
    successMessage: '',
    errorMessage: '',
    errors: {},
  })
  const onUserSelect = event => {
    const listItem = event.target

    setExecutors(prevState => {
      prevState = new Set(prevState)
      const value = parseInt(listItem.dataset.value)
      if (!listItem.classList.contains('active')) prevState.add(value)
      else prevState.delete(value)

      return prevState
    })
  }

  const onSuccess = response => {
    if (response.data && response.data.success) {
      setStatus({
        successMessage: 'Task has been created',
        errorMessage: '',
        errors: {},
      })
      setTitle('')
      setDescription('')
      setExecutors(new Set())
      setAttachment('')
      loadTasks()
    } else
      setStatus({
        successMessage: '',
        errorMessage: 'Something is wrong',
        errors: {},
      })
  }

  const onError = error => {
    const errorResponse = error.response
    if (errorResponse && errorResponse.data) {
      if (errorResponse.data.errors) {
        setStatus({
          successMessage: '',
          errorMessage: '',
          errors: errorResponse.data.errors,
        })
      } else {
        setStatus({
          successMessage: '',
          errorMessage: errorResponse.data.message || 'Server error!',
          errors: {},
        })
      }
    } else {
      setStatus({
        successMessage: '',
        errorMessage: "Task hasn't been created",
        errors: {},
      })
    }
  }

  const handleSubmit = event => {
    createTask(
      token,
      user.id,
      title,
      description,
      attachment.name,
      Array.from(executors),
      onSuccess,
      onError
    )
  }

  const addAttachment = file => setAttachment(file)
  const dropAttachment = id => setAttachment('')

  return (
    <form>
      <h5>Create a new task</h5>
      {status.successMessage && (
        <MDBAlert color="success">{status.successMessage}</MDBAlert>
      )}
      {status.errorMessage && (
        <MDBAlert color="danger">{status.errorMessage}</MDBAlert>
      )}
      <MDBInput
        label="Title"
        value={title}
        onChange={event => setTitle(event.target.value)}
      />
      {status.errors.title && (
        <div className="invalid__feedback">{status.errors.title[0]}</div>
      )}
      <MDBInput
        label="Description"
        value={description}
        onChange={event => setDescription(event.target.value)}
      />
      {status.errors.description && (
        <div className="invalid__feedback">{status.errors.title[0]}</div>
      )}
      <div className="form-group">
        <AttachmentDropzone
          manuallyRedraw={() => {}}
          {...{
            addAttachment,
            dropAttachment,
            disableIf: files => files.length >= 1,
            multiple: false,
            token,
          }}
        />
        {status.errors.picture_url && (
          <div className="invalid__feedback">
            {status.errors.picture_url[0]}
          </div>
        )}
      </div>
      <UsersList
        {...{ onUserSelect, members: executors, title: 'Executors:' }}
      />
      {status.errors.executors && (
        <div className="invalid__feedback">{status.errors.executors[0]}</div>
      )}
      <div className="form-group">
        <MDBBtn size="block" color="success" onClick={handleSubmit}>
          Create
        </MDBBtn>
      </div>
    </form>
  )
})

NewTask.propsTypes = {
  user: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
  loadTasks: PropTypes.func.isRequired,
}

export default NewTask
