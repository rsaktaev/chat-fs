import React, { useState } from 'react'
import EmojiPicker from 'emoji-picker-react'

import PropTypes from 'prop-types'
import '../../../css/forms/NewMessage.css'
import { MDBInput, MDBAlert, MDBIcon } from 'mdbreact'
import { useDropzone } from 'react-dropzone'
import Attachment from './Attachment'
import { emojiInit } from '../../utils'

const NewMessage = props => {
  const [message, setMessage] = useState('')
  const [files, setFiles] = useState([])
  const [attachments, setAttachments] = useState([])
  const [epickerIsOpen, setEpickerIsOpen] = useState(false)

  let jsEmoji = emojiInit()
  jsEmoji.text_mode = true

  //on file drag
  const onDrop = acceptedFiles => {
    setFiles(prevFiles => {
      return [...prevFiles, ...acceptedFiles]
    })
  }

  //handle delete file from attachments
  const onDeleteFile = (event, index, id) => {
    event.preventDefault()
    setFiles(files => {
      return files.filter((el, i) => i !== index)
    })

    setAttachments(attachments => {
      return attachments.filter(el => el.id !== id)
    })
  }

  const { getRootProps, getInputProps, open } = useDropzone({
    onDrop: onDrop,
  })

  const handleSendMessage = event => {
    event.preventDefault()

    jsEmoji.text_mode = true
    props.sendMessage(message, attachments)
    setEpickerIsOpen(false)
    setMessage('')
    setFiles([])
    setAttachments([])
  }

  const onMessageChange = event => {
    const { user, channelpath } = props
    window.Echo.private(channelpath).whisper('typing', {
      userName: user.display_name,
    })
    setMessage(event.currentTarget.value)
  }

  const displayFiles = files => {
    return files.map((file, index) => {
      return (
        <Attachment
          key={file.path}
          index={index}
          onDeleteFile={onDeleteFile}
          token={props.token}
          file={file}
          addAttachment={file =>
            setAttachments(prevState => [...prevState, file])
          }
          dropAttachment={id =>
            setAttachments(prevState => prevState.filter(el => el.id !== id))
          }
        />
      )
    })
  }

  const onEmojiClick = (code, emoji) => {
    let emojiPic = jsEmoji.replace_colons(`:${emoji.name}:`)
    setMessage(text => text + emojiPic)
  }

  return (
    <div
      className="new__message__form"
      {...getRootProps({ onClick: event => event.stopPropagation() })}
    >
      <input {...getInputProps()} />
      <form onSubmit={handleSendMessage}>
        {props.errorMessage && (
          <MDBAlert className="mt-2" color="danger">
            {props.errorMessage}
          </MDBAlert>
        )}
        <div className="form__wrapper">
          <MDBInput
            icon="paper-plane"
            type="textarea"
            className="pb-0"
            label="Send new message"
            rows="1"
            value={message}
            onChange={onMessageChange}
          />
          {/** emoji picker if opened */}
          {epickerIsOpen && (
            <div className="emoji_picker">
              <EmojiPicker onEmojiClick={onEmojiClick} />
            </div>
          )}
          {/**opens an emoji picker */}
          <div
            className="form_button"
            onClick={() => setEpickerIsOpen(prevState => !prevState)}
          >
            <MDBIcon far icon="smile" />
          </div>
          {/** adds attachments */}
          <div className="form_button" onClick={() => open()}>
            <MDBIcon icon="paperclip" />
          </div>
          {/**sends a message */}
          {props.isLoading ? (
            <div className="form_button">
              <div className="spinner-border">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          ) : (
            <div
              className="form_button"
              onClick={event => handleSendMessage(event)}
            >
              <MDBIcon far icon="paper-plane" />
            </div>
          )}
        </div>
        <div className="attachments_list">{displayFiles(files)}</div>
      </form>
    </div>
  )
}

NewMessage.propTypes = {
  user: PropTypes.object.isRequired,
  channelpath: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  token: PropTypes.string.isRequired,
  sendMessage: PropTypes.func.isRequired,
}

export default NewMessage
