import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import '../../../css/forms/AttachmentDropzone.css'
import { useDropzone } from 'react-dropzone'
import Attachment from './Attachment'

const AttachmentDropzone = ({
  manuallyRedraw,
  addAttachment,
  dropAttachment,
  token,
  disableIf = () => false,
  multiple = true,
}) => {
  const [files, setFiles] = useState([])

  useEffect(() => {
    manuallyRedraw()
  })

  //on file drag
  const onDrop = acceptedFiles => {
    setFiles(prevFiles => {
      return [...prevFiles, ...acceptedFiles]
    })
  }

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: onDrop,
    disabled: disableIf(files),
    multiple,
  })

  //handle delete file from attachments
  const onDeleteFile = (event, index, id) => {
    event.preventDefault()
    event.stopPropagation()
    setFiles(files => {
      return files.filter((el, i) => i !== index)
    })

    dropAttachment(id)
  }

  const displayFiles = files => {
    return files.map((file, index) => {
      return (
        <Attachment
          key={file.path}
          index={index}
          onDeleteFile={onDeleteFile}
          token={token}
          file={file}
          checkError={file => /^image\/.*$/.test(file.type)}
          addAttachment={addAttachment}
        />
      )
    })
  }

  return (
    <div className="attachment-dz" {...getRootProps()}>
      <input {...getInputProps()} />
      {files.length ? (
        <div className="attachments_list">{displayFiles(files)}</div>
      ) : (
        <div className="caption">Add your files here</div>
      )}
    </div>
  )
}

AttachmentDropzone.propTypes = {
  manuallyRedraw: PropTypes.func.isRequired,
  addAttachment: PropTypes.func.isRequired,
  dropAttachment: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  disableIf: PropTypes.func,
  multiple: PropTypes.bool,
}

export default AttachmentDropzone
