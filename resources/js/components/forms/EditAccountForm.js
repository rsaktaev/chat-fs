import React from 'react'
import PropTypes from 'prop-types'

import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBInput,
  MDBBtn,
  MDBIcon,
  MDBAlert,
} from 'mdbreact'
import { Link } from 'react-router-dom'
import { loadUserInfo, updateUser, deleteUser } from '../../api'
import {
  UPDATE_SUCCESS,
  UPDATE_ERROR,
  DELETE_SUCCESS,
  DELETE_ERROR,
} from '../../constants'

class EditAccountForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      display_name: '',
      full_name: '',
      email: '',
      phone: '',
      avatar_url: '',
      notFound: false,
      shouldFetch: false,
      errors: {},
      alertMessage: '',
      status: '',
      isLoading: false,
      needsDerivedState: props.needsDerivedState,
    }
  }

  /**
   * If user update himself, derived state is required
   * otherwise needs fetch data
   *
   * @param {object} props
   * @param {object} state
   *
   * @returns {object} state
   */
  static getDerivedStateFromProps(props, state) {
    const user = props.user
    if (user && state.needsDerivedState) {
      return {
        id: user.id,
        display_name: user.display_name,
        full_name: user.full_name,
        email: user.email,
        phone: user.phone,
        avatar_url: user.avatar_url,
        shouldFetch: false,
        needsDerivedState: false,
      }
    }

    return { shouldFetch: true }
  }

  onLoadingUserInfoSuccess = res => {
    if (res.status === 200) {
      const { id, display_name, full_name, email, phone, avatar_url } = res.data
      this.setState({
        id: id,
        display_name: display_name,
        full_name: full_name,
        email: email,
        phone: phone,
        avatar_url: avatar_url,
        shouldFetch: false,
        needsDerivedState: false,
      })
    }
  }

  onLoadingUserInfoError = error => {
    if (!error.response) {
      console.log(error)
    } else if (error.response.status === 404)
      this.setState({ notFound: true, shouldFetch: false })
    else if (error.response.status === 401) this.props.logout()
  }

  /**
   * Async loading info about user
   *
   */
  loadUserInfo = () => {
    const { token, match } = this.props
    const id = match.params.id

    console.log(token)

    loadUserInfo(
      id,
      token,
      this.onLoadingUserInfoSuccess,
      this.onLoadingUserInfoError
    )
  }

  onUpdatingUserSuccess = response => {
    const { loadUsers, updateUserInfo } = this.props

    if (!response.data) {
      this.setState({
        alertMessage: response.data.error,
        status: UPDATE_ERROR,
        isLoading: false,
      })
    } else {
      if (response.data.id === this.props.loggedUser.id) {
        //if update logged user
        updateUserInfo(response.data)
      }
      loadUsers()
      this.setState({
        errors: {},
        alertMessage: 'All changes have been saved.',
        status: UPDATE_SUCCESS,
        isLoading: false,
      })
    }
  }

  onUpdatingUserError = error => {
    let response = error.response

    if (response && response.status === 401) {
      this.props.logout()
    } else {
      if (response.data.errors) {
        this.setState({
          errors: response.data.errors,
          status: '',
          isLoading: false,
        })
      } else {
        this.setState({
          alertMessage: 'Server error',
          status: UPDATE_ERROR,
          isLoading: false,
        })
      }
    }
  }

  /**
   * Async delete request
   *
   * @param {object} event
   */
  handleSubmit = event => {
    event.preventDefault()
    this.setState({ isLoading: true })

    const { id, display_name, full_name, email, phone, avatar_url } = this.state
    const { loggedUser, token } = this.props
    updateUser(
      { id, display_name, full_name, email, phone, avatar_url },
      token,
      loggedUser,
      this.onUpdatingUserSuccess,
      this.onUpdatingUserError
    )
  }

  onDeletingUserSuccess = response => {
    if (response.data) {
      //update state in the App comonent
      this.props.loadUsers()
      this.setState({
        errors: {},
        status: DELETE_SUCCESS,
        alertMessage: 'User has been deleted successfully',
        isLoading: false,
      })
    } else {
      this.setState({
        status: DELETE_ERROR,
        alertMessage: response.data.error,
        isLoading: false,
      })
    }
  }

  onDeletingUserError = error => {
    let response = error.response
    if (response && response.status === 401) {
      this.props.logout()
    } else {
      console.log(error)
      this.setState({
        status: DELETE_ERROR,
        alertMessage: 'Server error',
        isLoading: false,
      })
    }
  }

  /**
   * Async delete request
   *
   * @param {object} event
   */
  handleDelete = event => {
    event.preventDefault()
    this.setState({ isLoading: true })

    const { match, token } = this.props
    const id = match.params.id

    deleteUser(id, token, this.onDeletingUserSuccess, this.onDeletingUserError)
  }

  onInputChange = event =>
    this.setState({ [event.currentTarget.name]: event.currentTarget.value })

  topAlert = (status, alertMessage) => {
    if (status === DELETE_ERROR || status === UPDATE_ERROR)
      return <MDBAlert color="danger">{alertMessage}</MDBAlert>
    if (status === UPDATE_SUCCESS)
      return <MDBAlert color="success">{alertMessage}</MDBAlert>
  }

  render() {
    const {
      id,
      display_name,
      full_name,
      email,
      phone,
      avatar_url,
      notFound,
      alertMessage,
      errors,
      status,
      isLoading,
    } = this.state
    const { loggedUser, backUrl } = this.props

    return (
      <MDBContainer className="settings__subcontainer">
        {notFound ? (
          //TODO- 404 Error component
          <div>User not found</div>
        ) : (
          <React.Fragment>
            <Link to={backUrl}>
              <MDBIcon icon="arrow-left" size="2x" />
            </Link>
            {this.topAlert(status, alertMessage)}
            {status === DELETE_SUCCESS ? (
              <MDBAlert color="success">{alertMessage}</MDBAlert>
            ) : (
              <form>
                <MDBRow>
                  <MDBCol md="3" size="6" className="offset-md-0 offset-3">
                    <MDBContainer>
                      <div className="rounded-circle">
                        <img
                          className="rounded-circle img-fluid"
                          src={avatar_url}
                          alt="user avatar"
                        />
                      </div>
                    </MDBContainer>
                  </MDBCol>
                  <MDBCol md="9">
                    <MDBContainer>
                      <MDBInput
                        type="text"
                        name="display_name"
                        value={display_name}
                        onChange={this.onInputChange}
                        icon="male"
                        label="Display name"
                      />
                      {errors.display_name && (
                        <div className="invalid__feedback">
                          {errors.display_name[0]}
                        </div>
                      )}
                      <MDBInput
                        type="text"
                        name="full_name"
                        value={full_name}
                        onChange={this.onInputChange}
                        icon="id-card"
                        label="Full name"
                      />
                      {errors.full_name && (
                        <div className="invalid__feedback">
                          {errors.full_name[0]}
                        </div>
                      )}
                      <MDBInput
                        type="text"
                        name="email"
                        value={email}
                        onChange={this.onInputChange}
                        icon="envelope"
                        label="Email"
                      />
                      {errors.email && (
                        <div className="invalid__feedback">
                          {errors.email[0]}
                        </div>
                      )}
                      <MDBInput
                        type="text"
                        name="phone"
                        value={phone}
                        onChange={this.onInputChange}
                        icon="mobile"
                        label="Phone number"
                      />
                      {errors.phone && (
                        <div className="invalid__feedback">
                          {errors.phone[0]}
                        </div>
                      )}
                    </MDBContainer>
                  </MDBCol>
                </MDBRow>
                <MDBRow>
                  <MDBCol size="12">
                    <MDBContainer>
                      <MDBBtn
                        type="submit"
                        color="primary"
                        block
                        onClick={this.handleSubmit}
                        disabled={isLoading}
                      >
                        Submit
                      </MDBBtn>
                      {loggedUser.id === id || (
                        <MDBBtn
                          type="submit"
                          className="mt-3"
                          color="danger"
                          block
                          disabled={isLoading}
                          onClick={this.handleDelete}
                        >
                          Delete
                        </MDBBtn>
                      )}
                    </MDBContainer>
                  </MDBCol>
                </MDBRow>
              </form>
            )}
          </React.Fragment>
        )}
      </MDBContainer>
    )
  }

  componentDidMount() {
    if (this.state.shouldFetch) this.loadUserInfo()
  }
}

EditAccountForm.propTypes = {
  backUrl: PropTypes.string.isRequired,
  user: PropTypes.object,
  token: PropTypes.string.isRequired,
  loggedUser: PropTypes.object.isRequired,
  loadUsers: PropTypes.func.isRequired,
  updateUserInfo: PropTypes.func.isRequired,
  needsDerivedState: PropTypes.bool.isRequired,
}

export default EditAccountForm
