import React from 'react'
import PropTypes from 'prop-types'

import { MDBInput, MDBBtn } from 'mdbreact'

class LoginForm extends React.Component {
  state = {
    phone: '77777777777', //test
    password: '',
  }

  handleChange = event => {
    let { name, value } = event.currentTarget
    this.setState({ [name]: value })
  }

  handleSubmit = event => {
    event.preventDefault()
    const { phone, password } = this.state

    this.props.onLogin(phone, password)
  }

  render() {
    const { phone, password } = this.state
    const { handleChange, handleSubmit } = this

    const errors = this.props.errors
    return (
      <form onSubmit={handleSubmit} className="needs-validation" noValidate>
        <MDBInput
          type="tel"
          pattern="[0-9]{11}"
          label="Phone number"
          name="phone"
          value={phone}
          className="is-invalid"
          onChange={handleChange}
        />
        {errors.phone && (
          <div className="invalid__feedback">{errors.phone[0]}</div>
        )}
        <MDBInput
          type="password"
          label="Password"
          name="password"
          value={password}
          onChange={handleChange}
        />
        {errors.password && (
          <div className="invalid__feedback">{errors.password[0]}</div>
        )}

        <MDBBtn type="submit" block>
          Login
        </MDBBtn>
      </form>
    )
  }
}

LoginForm.propTypes = {
  history: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onLogin: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
}

export default LoginForm
