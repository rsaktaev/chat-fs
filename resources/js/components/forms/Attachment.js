import React, { useEffect, useState } from 'react'
import { loadToServer } from '../../api'
import PropTypes from 'prop-types'
import { shortcutString } from '../../utils'
import { MDBIcon, MDBBtn } from 'mdbreact'
import '../../../css/forms/Attachment.css'

const Attachment = ({
  index,
  token,
  file,
  attachment,
  onDeleteFile,
  addAttachment,
  checkError = file => true,
}) => {
  const [status, setStatus] = useState('loading')
  const [fileEntity, setFileEntity] = useState({})

  const _onSuccess = response => {
    if (response.data.success) {
      setFileEntity(response.data.attachment)
      addAttachment(response.data.attachment)
      setStatus('success')
    } else {
      setStatus('failure')
    }
  }

  const _onError = error => {
    setStatus('failure')
  }

  useEffect(() => {
    const formData = new FormData()
    formData.append('attachments', file, file.name)
    if (checkError(file)) loadToServer(formData, token, _onSuccess, _onError)
    else _onError()
  }, [])

  const renderSymbol = () => {
    switch (status) {
      case 'loading':
        return (
          <div className="file__loader">
            <div className="spinner-border">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )
      case 'success':
        return <MDBIcon icon="check" />
      case 'failure':
        return <MDBIcon icon="exclamation" />
      default:
        return false
    }
  }
  return (
    <div className="attachment">
      <div className={`status_symbol ${status}`}>{renderSymbol()}</div>

      <div className="filename">
        {status === 'success' ? (
          <a
            href={`/img/attachments/${fileEntity.name}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {shortcutString(fileEntity.name, 5)}
          </a>
        ) : (
          shortcutString(file.name, 5)
        )}
      </div>
      <MDBBtn onClick={event => onDeleteFile(event, index, fileEntity.id)}>
        x
      </MDBBtn>
    </div>
  )
}

Attachment.propTypes = {
  index: PropTypes.number.isRequired,
  token: PropTypes.string.isRequired,
  file: PropTypes.object.isRequired,
  onDeleteFile: PropTypes.func.isRequired,
  checkError: PropTypes.func,
  addAttachment: PropTypes.func.isRequired,
}

export default Attachment
