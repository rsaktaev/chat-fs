import { initSW } from '../../js/service_worker/enable-push'

import React from 'react'
import ReactDOM from 'react-dom'
import { Switch, withRouter } from 'react-router-dom'
import HomePage from './pages/HomePage'
import SettingsPage from './pages/SettingsPage'
import LoginPage from './pages/LoginPage'

import {
  login,
  logout,
  loadConversations,
  loadUsers,
  slackChannels,
} from '../api'
import { MDBAlert } from 'mdbreact'
import { BrowserRouter } from 'react-router-dom'

import '../../css/app.css'

import { AuthRoute, NonAuthRoute } from './route_components'

export class App extends React.Component {
  state = {
    user: {},
    token: '',
    usersList: { users: [], size: 0 },
    conversations: { conversations: [], size: 0 },
    loginIsLoading: false,
    isLoading: true,

    loginErrorMessage: '', // message for any other login error reponses
    loginErrors: {}, // error messages for failed validation response
    loadingError: '',
  }

  setStatus = state => this.setState(state)

  onSuccessLogin = res => {
    const responseData = res.data

    //if login successful, we are store app state to localstorage
    if (!responseData.error) {
      localStorage.setItem('token', responseData.token)
      localStorage.setItem('user', JSON.stringify(responseData.user))

      this.setState({
        ...responseData,
        loginErrorMessage: '', //clear errors
        loginErrors: {},
        loginIsLoading: false,
      })

      initSW()
      this.loadUsers()
      this.loadConversations()

      const path = sessionStorage.getItem('intendedUrl') || '/' // returns intended path
      sessionStorage.removeItem('intendedUrl')
      this.props.history.push(path) //go to intended path
    } else {
      //otherwise we are show error alert (e.g. - invalid credentials)
      this.setState({
        loginErrorMessage: `${res.data.message || 'Login failed'}`,
        loginIsLoading: false,
      })
    }
  }

  onErrorLogin = error => {
    // handle error responses from server
    if (error.response) {
      const responseData = error.response.data
      const errors = responseData.errors
      if (errors) {
        // if validation falls
        this.setState({ loginErrors: errors, loginIsLoading: false })
      } else {
        // another some server error
        console.log(error.response)
        this.setState({
          loginErrorMessage: 'Server error.',
          loginIsLoading: false,
        })
      }
    } else {
      this.setState({
        loginErrorMessage: 'Something is wrong', //errors in promise
        loginIsLoading: false,
      })
    }
  }

  /**
   * Send request to login user
   *
   * @param {string} phone
   * @param {string} password
   */
  onLogin = (phone, password) => {
    this.setState({ loginIsLoading: true })
    login(phone, password, this.onSuccessLogin, this.onErrorLogin)
  }

  _onLogout = res => {
    localStorage.removeItem('token') //clear local storage from app state
    localStorage.removeItem('user')
    this.setState({ user: {} })
    this.props.history.push('/login')
  }

  /**
   * Send request to logout
   *
   * */
  onLogout = () =>
    logout()
      .then(this._onLogout)
      .catch(this._onLogout)

  onLoadConversationsSuccess = response => {
    const responseData = response.data
    if (!responseData.error) {
      this.setState({
        conversations: {
          conversations: responseData.conversations,
          size: responseData.size,
        },
        loadingError: null,
      })
    } else {
      this.onLogout()
    }
  }

  onLoadConversationsError = error => {
    if (error.response && error.response.status === 401) {
      this.onLogout()
    } else {
      this.setState({
        loadingError: 'Server error',
      })
    }
  }

  loadConversations = skip => {
    loadConversations(
      this.onLoadConversationsSuccess,
      this.onLoadConversationsError,
      skip
    )
  }

  onLoadingUsersSuccess = res => {
    const responseData = res.data
    if (!responseData.error) {
      this.setState({
        usersList: responseData,
        loadingError: null,
        isLoading: false,
      })
    } else {
      this.setState({
        loadingError: null,
        isLoading: false,
      })
      this.onLogout()
    }
  }

  onLoadingUsersError = error => {
    console.log(error.response)
    if (error.response && error.response.status === 401) {
      this.setState({
        loadingError: null,
        isLoading: false,
      })
      this.onLogout()
    } else {
      this.setState({
        loadingError: 'Server error',
        isLoading: false,
      })
    }
  }

  /**
   * Loading users list from server
   * */
  loadUsers = () => {
    this.setState({ isLoading: true })
    loadUsers(this.onLoadingUsersSuccess, this.onLoadingUsersError)
  }

  /**
   * Update state after edit user's data
   *
   * @param {object} user
   *
   * */

  updateUserInfo = user => {
    localStorage.setItem('user', JSON.stringify(user))
    this.setState({ user })
  }

  render() {
    const {
      user,
      token,
      usersList,
      conversations,
      loginIsLoading,
      isLoading,
      loginErrorMessage,
      loginErrors,
      loadingError,
    } = this.state

    return (
      <React.Fragment>
        {loadingError ? (
          <MDBAlert color="danger">{loadingError}</MDBAlert>
        ) : (
          <Switch>
            <AuthRoute
              path="/settings"
              render={props =>
                token && (
                  <SettingsPage
                    {...props}
                    user={user}
                    token={token}
                    onDeleteUser={this.onDeleteUser}
                    usersList={usersList}
                    logout={this.onLogout}
                    loadUsers={this.loadUsers}
                    updateUserInfo={this.updateUserInfo}
                  />
                )
              }
            />
            <NonAuthRoute
              path="/login"
              component={props => (
                <LoginPage
                  {...props}
                  onLogin={this.onLogin}
                  isLoading={loginIsLoading}
                  errorMessage={loginErrorMessage}
                  errors={loginErrors}
                />
              )}
            />
            <AuthRoute
              path="/"
              render={props => (
                <HomePage
                  {...props}
                  user={user}
                  token={token}
                  loadUsers={this.loadUsers}
                  loadConversations={this.loadConversations}
                  isLoading={isLoading}
                  usersList={usersList}
                  setStatus={this.setStatus}
                  conversations={conversations}
                  onLogout={this.onLogout}
                />
              )}
            />
          </Switch>
        )}
      </React.Fragment>
    )
  }

  onLoadingChannelsSuccess = response => {
    if (response.data) {
      const cursor = response.data.response_metadata.next_cursor
      if (cursor)
        slackChannels(
          this.state.token,
          this.onLoadingChannelsSuccess,
          error => console.log(error.response),
          cursor
        )
      else console.log(response)
    }
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user')) || {}
    const token = localStorage.getItem('token')

    if (token) {
      this.setState({ user, token })
      this.loadUsers()
      this.loadConversations()
    }
  }
}

const AppWithRouter = withRouter(App)

if (document.getElementById('root')) {
  ReactDOM.render(
    <BrowserRouter>
      <AppWithRouter />
    </BrowserRouter>,
    document.getElementById('root')
  )
}
