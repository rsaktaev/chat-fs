import EmojiConverter from 'emoji-js'

export const shortcutString = (string, length) => {
  const dots = string.length > length ? '...' : ''
  return `${string.slice(0, length)}${dots}`
}

export const formattedTime = date => {
  let hours = date.getHours(),
    minutes = date.getMinutes(),
    seconds = date.getSeconds()

  if (hours < 10) hours = '0' + hours
  if (minutes < 10) minutes = '0' + minutes
  if (seconds < 10) seconds = '0' + seconds

  return `${hours}:${minutes}:${seconds}`
}

export const emojiInit = () => {
  let jsEmoji = new EmojiConverter()

  jsEmoji.img_set = 'emojione'
  jsEmoji.img_sets.emojione.path =
    'https://cdn.jsdelivr.net/emojione/assets/3.0/png/32/'

  return jsEmoji
}

//converts to links
export const formatMessage = message => {
  const emoji = emojiInit()
  return message
    .replace(
      /((http|https|ftp):\/\/){0,1}[A-Za-zА-яа-я0-9.-]+\.([A-Za-z]{2,}|рф)(\/[^\n\s]*)*/g,
      match => {
        return `<a href="${
          /^(http|https|ftp):\/\/.*/.test(match) ? match : `http://${match}`
        }" target="_blank">${match}</a>`
      }
    )
    .replace(/\n/g, '<br/>')
    .replace(/:.+:/g, match => emoji.replace_colons(match))
}
