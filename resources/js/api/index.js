/*******
 * This file contains all of async requests
 */

import axios from 'axios'

/**
 * Send request to login user
 *
 * @param {string} phone
 * @param {string} password
 * @callback onSuccess
 * @callback onError
 */
export const login = (phone, password, onSuccess, onError) =>
  axios
    .post('/api/login', {
      phone: phone,
      password: password,
    })
    .then(res => onSuccess(res))
    .catch(error => onError(error))

/**
 * Send request to logout
 */
export const logout = () =>
  axios({
    method: 'get',
    url: '/api/logout',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  })

/**
 * Loading list of conversations
 *
 * @callback onSuccess
 * @callback onError
 */
export const loadConversations = (onSuccess, onError, skip) => {
  const token = localStorage.getItem('token')

  axios({
    method: 'get',
    url: `/api/conversations/my`,
    params: { skip },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 * Loading users list from server
 *
 * @callback onSuccess
 * @callback onError
 * @param {number} skip
 * @param {string} pattern
 */
export const loadUsers = (onSuccess, onError, skip, pattern) => {
  const token = localStorage.getItem('token')

  axios({
    method: 'get',
    url: `/api/users`,
    params: { skip, pattern },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 * Loading messages for a channel
 *
 * @param {string} token
 * @param {number} id
 * @callback onSuccess
 * @callback onError
 */
export const loadMessagesForChannel = (token, id, onSuccess, onError) => {
  axios({
    method: 'get',
    url: `/api/conversations/${id}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => {
      onSuccess(response)
    })
    .catch(error => {
      onError(error)
    })
}

/**
 * Sending message for a channel
 *
 * @param {number} channelId
 * @param {object} user
 * @param {string} token
 * @param {string} text
 * @param {array} attachments
 * @callback onSuccess
 * @callback onError
 */
export const sendMessageToChannel = (
  channelId,
  user,
  token,
  text,
  attachments,
  onSuccess,
  onError
) => {
  axios({
    method: 'post',
    url: `/api/conversations/${channelId}/send`,
    data: {
      sender_id: user.id,
      text,
      attachments,
      conversation_id: channelId,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 * @param {number} user_id
 * @param {string} title
 * @param {array} members
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const createChannel = (
  user_id,
  title,
  members,
  token,
  onSuccess,
  onError
) =>
  axios({
    method: 'post',
    url: '/api/conversations',
    data: {
      user_id,
      title,
      members,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Loading messages with user
 *
 * @param {Object} interlocutor
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const loadDialogMessages = (interlocutor, token, onSuccess, onError) => {
  axios({
    method: 'get',
    url: `/api/messages/${interlocutor.id}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 * Loading userinfo and messages
 *
 * @param {number} interlocutorId
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const loadDialogInfo = (interlocutorId, token, onSuccess, onError) =>
  axios({
    mathod: 'get',
    url: `/api/users/${interlocutorId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Sending a new message
 *
 * @param {number} id
 * @param {string} text
 * @param {object} attachments
 * @param {object} user
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const sendMessageToUser = (
  interlocutorId,
  text,
  attachments,
  user,
  token,
  onSuccess,
  onError
) => {
  axios({
    method: 'post',
    url: `/api/messages`,
    data: {
      sender_id: user.id,
      receiver_id: interlocutorId,
      text,
      attachments,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => {
      onSuccess(response)
    })
    .catch(error => {
      onError(error)
    })
}

/**
 * Async method loads channels list from server
 *
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 * @param {number} skip
 * @param {string} search
 */
export const loadChannels = (token, onSuccess, onError, skip, search) => {
  axios({
    method: 'get',
    url: '/api/conversations',
    params: {
      skip,
      search,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 * Adding members in channel
 *
 * @param {number} conversationId
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const toggleChannel = (channelId, token, onSuccess, onError) =>
  axios({
    method: 'put',
    url: `/api/conversations/${channelId}/toggle`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Adding members in channel
 *
 * @param {number} conversationId
 * @param {array} members
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const inviteChannel = (channelId, members, token, onSuccess, onError) =>
  axios({
    method: 'put',
    url: `/api/conversations/${channelId}/invite`,
    data: { members },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Removing members in channel
 *
 * @param {number} conversationId
 * @param {array} members
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const removeChannel = (channelId, members, token, onSuccess, onError) =>
  axios({
    method: 'put',
    url: `/api/conversations/${channelId}/remove`,
    data: { members },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Async method creates new user
 *
 * @param {object} userData
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const createUser = (
  { display_name, full_name, email, phone },
  token,
  onSuccess,
  onError
) => {
  axios({
    method: 'post',
    url: `/api/users`,
    data: {
      display_name: display_name,
      full_name: full_name,
      email: email,
      phone: phone,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 * Loading file on the server
 *
 * @param {*} formData
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const loadToServer = (formData, token, onSuccess, onError) =>
  axios({
    method: 'post',
    url: '/api/attachments',
    data: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Async loading info about user
 *
 * @param {number} id
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const loadUserInfo = (id, token, onSuccess, onError) => {
  axios({
    method: 'get',
    url: `/api/users/${id}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 *  Method updates user
 *
 * @param {*} userData
 * @param {string} token
 * @param {object} loggedUser
 * @callback onSuccess
 * @callback onError
 */
export const updateUser = (
  { id, display_name, full_name, email, phone, avatar_url },
  token,
  loggedUser,
  onSuccess,
  onError
) => {
  axios({
    method: 'put',
    url: `/api/users/${id}?token=${loggedUser.auth_token}`,
    data: {
      _method: 'PUT',
      id: id,
      display_name: display_name,
      full_name: full_name,
      email: email,
      phone: phone,
      avatar_url: avatar_url,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
}

/**
 *  Method deletes user
 *
 * @param {number} id
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const deleteUser = (id, token, onSuccess, onError) =>
  axios({
    method: 'delete',
    url: `/api/users/${id}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**********SLACK METHODS********* */
/**
 * @param {number} channelId
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const createSlackChannel = (channelId, token, onSuccess, onError) =>
  axios({
    method: 'post',
    url: '/api/slack/channels/new',
    data: {
      channelId,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 *
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 */
export const slackChannels = (token, onSuccess, onError, next, limit = 10) =>
  axios({
    method: 'get',
    url: '/api/slack/channels',
    params: { limit, next },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Import tasks list
 *
 * @param {string} token
 * @callback onSuccess
 * @callback onError
 *
 */
export const loadTasks = (token, onSuccess, onError) =>
  axios({
    method: 'get',
    url: '/api/tasks',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Creating task
 * @param {string} token
 * @param {string} title
 * @param {string} description
 * @param {string} picture_url
 * @param {array} executors
 * @callback onSuccess
 * @callback onError
 */
export const createTask = (
  token,
  creator_id,
  title,
  description,
  picture_url,
  executors,
  onSuccess,
  onError
) =>
  axios({
    url: '/api/tasks',
    method: 'post',
    data: {
      creator_id,
      title,
      description,
      picture_url,
      executors,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Task has been completed
 * @param {string} token
 * @param {number} taskId
 * @param {string} description
 * @param {object} attachment
 * @callback onSuccess
 * @callback onError
 */
export const completeTask = (
  token,
  taskId,
  description,
  attachments,
  onSuccess,
  onError
) =>
  axios({
    method: 'put',
    url: `/api/tasks/${taskId}/complete`,
    data: {
      description,
      attachments,
    },
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))

/**
 * Sets messages as readed
 * @param {string} token
 * @param {number} userId
 * @callback onSuccess
 * @callback onError
 */

export const setAsReaded = (token, userId, onSuccess, onError) =>
  axios({
    method: 'put',
    url: `/api/messages/readed/${userId}`,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => onSuccess(response))
    .catch(error => onError(error))
